
#define ALLOCATE

#include "dllib.h"

extern char DLtest_keys [];

main (int argc, char ** argv)
 
{
char * pt;
char fname [100], * arg;
int done = 0;

   if (argc == 1)
   {
      printf ("usage: play msg_file_name\n");
      return (0);
   }
 
   if (! DLInit ("dxxxB1C1", 10))
   {
      fprintf (stderr, "Error initializing the DiaLogic board\n");
      exit (0);
   }
 
   DLInstallQuit ();
   
   arg = argv [1];
   
   printf ("\n\nPhone the DCM system to hear this recording.\n");
   printf (" (press ^C to cancel)\n");

   for (;;)
      if (DLWaitCall (100))
         break;

   if (strlen (arg) > 4) /* might have a .vox */
   {
      if (! strcmp (".vox", arg + strlen (arg) - 4)) /* yup */
      {
         printf ("\nEnter (phone) a multi digit code and # to terminate message early\n");
      
         DLSetPlayTerm (PLAY_TERM_POUND_KEY);
          
         DLPlay (arg);
       
         done = 1;
      }
   }
   
   if (! done)
   {
       printf ("\nPress any (phone) key terminate message early\n");
       
       sprintf (fname, "%s.vox", arg);
          
       DLPlay (fname);
       printf ("               Keys = %s\n", DLtest_keys);
       printf ("               DLGetKeyString = %s\n", DLGetKeyString ());
       
       DLPlay (fname);
       printf ("               Keys = %s\n", DLtest_keys);
       printf ("               DLGetKeyString = %s\n", DLGetKeyString ());
       
       DLPlay (fname);
       printf ("               Keys = %s\n", DLtest_keys);
       printf ("               DLGetKeyString = %s\n", DLGetKeyString ());
       
       DLPlay (fname);
       printf ("               Keys = %s\n", DLtest_keys);
       printf ("               DLGetKeyString = %s\n", DLGetKeyString ());
       
   }
 
   pt = DLGetKeyString ();
        
   if (pt)
      if (strlen (pt))
         printf ("key string = %s\n", pt);
      else
         printf ("No key string entered\n");
             
   DLCleanup (1);
}
 
 
