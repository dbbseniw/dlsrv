
#define ALLOCATE

#include <stdio.h>
#include "dllib.h"
#include <sch.h>

#define MAINT_KEY "888"
#define INQ_KEY   "999"

typedef struct Sch_DL_struct
{
   char keybuf_C [100];
   int sch_index_I;
   int callok;
} SDS, * SDSPTR;

SDS sds;

Stat (char * pt)

{

   printf ("%s\n", pt);
   fflush (stdout);
}


SDLPlay (char * vox, char * txt)

{
   if (DLCheckTerm ())                           /* call is terminated */
   {
      sds.callok = FALSE;
      return;
   }

   Stat (txt);
   DLPlay (vox);

   if (DLCheckTerm ())                           /* call is terminated */
   {
      sds.callok = FALSE;
   }
}


int SayDigits (char * dig)

{

   for (;* dig ;dig ++)
   {
      switch (* dig)
      {
         case '0':
           SDLPlay ("md0.vox", "0");
           break;
           
         case '1':
           SDLPlay ("md1.vox", "1");
           break;
           
         case '2':
           SDLPlay ("md2.vox", "2");
           break;
           
         case '3':
           SDLPlay ("md3.vox", "3");
           break;
           
         case '4':
           SDLPlay ("md4.vox", "4");
           break;
           
         case '5':
           SDLPlay ("md5.vox", "5");
           break;
           
         case '6':
           SDLPlay ("md6.vox", "6");
           break;
           
         case '7':
           SDLPlay ("md7.vox", "7");
           break;
           
         case '8':
           SDLPlay ("md8.vox", "8");
           break;
           
         case '9':
           SDLPlay ("md9.vox", "9");
           break;
         
         default:
           break;
      }
   }
}


int KeyMatch (char * key)

{
char * pt, * strchr ();

   pt = DLGetKeyString ();
   
   if (* pt)
   {
      strcpy (sds.keybuf_C, pt);
      
      pt = strchr (sds.keybuf_C, POUND_KEY);
      
      if (pt)
         * pt = (char) 0;
      
      return (! strcmp (sds.keybuf_C, key));
   }
   
   return (FALSE);
}


char TermKey ()

{
char * pt;

   pt = DLGetKeyString ();
   
   if (* pt)
   {
      return (* (pt + strlen (pt) -1));
   }
   
   return (NULL);
}



int Maint ()

{

   DLSetPlayTerm (PLAY_TERM_POUND_KEY);
   SDLPlay ("m16.vox", "Maintenance ID#");
   
}


int ValidSchCode ()

{

   return (FALSE);
}


int Inq ()

{
int try = 0;

   for (;try < 3; try ++)
   {
      DLSetPlayTerm (PLAY_TERM_POUND_KEY);
      SDLPlay ("m8.vox", "Sch or Bus Code#");
   
      if (ValidSchCode ())
      {
     
         return (SUCCESS);
      }
      
      SDLPlay ("m26.vox", "Invalid ID Code");
   }
}


int ValidSecCode ()

{

   return (FALSE);
}


ReqPriClosingCode ()

{
}


ReqSecClosingCode ()

{
}


ReqSecCode ()

{
   
   DLSetPlayTerm (PLAY_TERM_POUND_KEY);
   SDLPlay ("m3.vox", "Enter security code");
   
   if (ValidSecCode ())
   {
      ReqPriClosingCode ();
      
      ReqSecClosingCode ();
   }
}


ValidAccess ()

{

   SayDigits (sds.keybuf_C);
   
   DLSetPlayTerm (PLAY_TERM_POUND_KEY | PLAY_TERM_STAR_KEY);
   SDLPlay ("m14.vox", "correct: #, err:= *");
   
   if (TermKey () == POUND_KEY)
   {
      ReqSecCode ();
   }
}


int IsValidAccess ()

{
   /* compare to keybuf_C */
   
   return (TRUE);
}


int Branch ()

{
int try = 0;

   for (; sds.callok && try < 3; try ++)
   {
      DLSetPlayTerm (PLAY_TERM_POUND_KEY);
      SDLPlay ("m18.vox", "Access code?");

#ifdef NOT_USED
      if (KeyMatch (MAINT_KEY))  /* 888 */
      {
         Maint ();
      }
      else
#endif
         if (KeyMatch (INQ_KEY))  /* 999 */
         {
            Inq ();
         }
         else
            if (IsValidAccess ())
            {
               ValidAccess ();
            }
            else
               SDLPlay ("m12.vox", "Invalid access code?");
   }            
   
   return (FALSE);
}


int NewCall ()

{

   sds.callok = TRUE;
   
   DLSetPlayTerm (PLAY_TERM_ANY_KEY);
   SDLPlay ("m1.vox", "Welcome to DCM");
   
   Branch ();
   
   SDLPlay ("m7.vox", "Good Bye");
}


void DLMain ()

{

   memset (sds.keybuf_C, NULL, sizeof (sds.keybuf_C));
   sds.sch_index_I = 0;
   sds.callok = FALSE;
   
   Stat ("\n\nWaiting for a caller ...\n(press ^C to cancel)");
   
   for (;;)
      if (DLWaitCall ())
         break;

   Stat ("New Caller");

   NewCall ();
   
   DLHangUp ();
}

#ifdef UNUSED
   if (strlen (arg) > 4) /* might have a .vox */
   {
      if (! strcmp (".vox", arg + strlen (arg) - 4)) /* yup */
      {
         printf ("\nEnter (phone) a multi digit code and # to terminate message early\n");
      
         DLSetPlayTerm (PLAY_TERM_POUND_KEY);
          
         DLPlay (arg);
       
          done = 1;
      }
   }
   
   if (! done)
   {
       printf ("\nPress any (phone) key terminate message early\n");
       
       sprintf (fname, "%s.vox", arg);
          
       DLPlay (fname);
   }
 
   pt = DLGetKeyString ();
        
   if (pt)
      if (strlen (pt))
         printf ("key string = %s\n", pt);
      else
         printf ("No key string entered\n");
             
}
#endif


int Init ()

{
static char func [] = "Init";

char *ptr, *getenv();
int dfterr = FALSE;

   if((ptr = getenv("SCHOOLSETUP")) != NULL)      /* check for setup in env */
   {                                              /* found so save it */
      strcpy (sysdflt.ClientDir_C, ptr);
      BldFullName (TmpBuf_C, ptr, SCHOOLDFT, "");
   }
   else                                           /* error out if not found */
      exit_err (func, "SCHOOLSETUP not found in environment");


   ddp = LoadDftData (TmpBuf_C, TRUE);
   
}


Cleanup ()

{
}


main (int argc, char ** argv)
 
{
char * pt;
char fname [100];
int done = 0;

   if (Init)
   {
      if (DLInit ("dxxxB1C1", 0))
      {
         DLInstallQuit ();
   
         for (;;)
         {
            DLMain ();
         }
   
         DLCleanup (1);
      }
      else
      {
         fprintf (stderr, "Error initializing the DiaLogic board\n");
         fflush (stderr);
      }
      
      Cleanup ();
   }
}
 
 
