
#include "dllib.h"
#include <srllib.h>
#include <dxxxlib.h>

extern char port_C [100];
extern keylog_C [100];
extern int chdev, debug_I;
extern int wtring_rings_I;

extern int last_call_ret_I;

main (int argc, char ** argv)

{
char * pt;
int ret;

   for (ret = 1; ret < argc; ret ++)
      if (! strcmp (argv [ret], "-d"))
      {
         debug_I = 1;
         break;
      }

   chdev = dx_open ("dxxxB1C1", 0);
   
   if (debug_I)
   {
      printf ("open  ret  %d\n", chdev);
      fflush (stdout);
   }

   if (chdev != -1)
   {
      dx_sethook (chdev, DX_ONHOOK, EV_SYNC);
   
      printf ("Waiting for ring ... (20 sec max)\n");
   
      ret = dx_wtring (chdev, 1, DX_OFFHOOK, 20);
      printf ("wtring  ret  %d\n", ret);

      if (ret == -1)
      {
         pt = ATDV_ERRMSGP (chdev);
         printf ("ring wait err: %s\n", pt);
      }
      else               /* ring, answer */
      {
         AudioMain (argc, argv);
         printf ("call, offhook, calling Play\n");

         ret = Play (chdev, "greet.vox");
         
         printf ("Play returns, with %d, calling Record\n", ret);
         
         ret = Record (chdev, "new.vox");

         printf ("Record returns, with %d\n", ret);
         
         ret = Play (chdev, "thanks.vox");
      }
   }
}


