//
// xmllib.c    XML Decomposition functions
//

//#include "stdafx.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "malloc.h"




#define TRUE 1
#define FALSE 0

struct XML_NODE;

typedef struct XML_NODE
{
	char * pXmlTag;
	char * pData;
	struct XML_NODE * pChild;
	struct XML_NODE * pNext;
}XN, * XNPTR;

XNPTR pXNRoot;

int debugI = FALSE;

#define MAX_DATA_ITEMS  20000  //   ==   XML tags, xml text data, etc
				// jacked up to 20000, directories have MANY more items than expected

char * itemList [MAX_DATA_ITEMS];
int itemUsed [MAX_DATA_ITEMS];
int itemCount = 0;

int mallocsCount = 0;

char errBuf [100];


void Err (char * buf)

{

	fprintf (stderr, buf);
}


//#define min(x,y)      (x > y ? y : x)

char * strncpyn (char * to, char * from, int lim)

{
                                           /* copy either string + NULL */
                                           /* or lim and add NULL */
	int fromLen = strlen (from);

	if (fromLen + 1 < lim)
		lim = fromLen + 1;

   strncpy (to, from, lim);
   * (to + lim) = 0;

   return (from);
}



char * TagItem (char * dat)

{

	char * end = strchr (dat, '>');

	if (end)	// <tag>
	{
	int len = end - dat + 1;
	char * tmp = (char *)malloc (len + 1);
		mallocsCount ++;

		strncpyn (tmp, dat, len);

		itemList [itemCount ++] = tmp;
		return (dat + len);
	}
	else	// data
	{
	int len = strlen (dat);
	char * tmp = (char *) malloc (len + 1);
		mallocsCount ++;

		strncpyn (tmp, dat, len);

		itemList [itemCount ++] = tmp;

		return (dat + len);
	}
}


char * TextItem (char * dat)

{

	char * end = strchr (dat, '<');
	if (end)	// new <tag>
	{
	int len = end - dat;
	char * tmp = (char *)malloc (len + 1);
		mallocsCount ++;

		strncpyn (tmp, dat, len);

		itemList [itemCount ++] = tmp;
		return (dat + len);
	}
	else	// data to end
	{
	int len = strlen (dat);
	char * tmp = (char *) malloc (len + 1);
		mallocsCount ++;

		strncpyn (tmp, dat, len);

		itemList [itemCount ++] = tmp;

		return (dat + len);
	}
}


int XNIsATag (int idx)

{

	return ((* itemList [idx]) == '<');
}


XNPTR NewXNPtr ()

{
char * tmp = (char *) malloc (sizeof (XN));
XNPTR pX = (XNPTR) tmp;
		mallocsCount ++;

	pX -> pXmlTag = 0;
	pX -> pData = 0;
	pX -> pChild = 0;
	pX -> pNext = 0;

	return (pX);
}


char * TextMemory (char * dat)

{
char * tmp;

	tmp = (char *) malloc (strlen (dat) + 1);
		mallocsCount ++;

	strcpy (tmp, dat);

	return (tmp);
}


char * XNBackTag (int idx)
{
static char bTag [50];
char bTagTmp [50];
int len = strlen (itemList [idx]) - 2;

	memcpy (bTagTmp, itemList [idx] + 1, len);
	bTagTmp [len] = 0;

	sprintf (bTag, "</%s>", bTagTmp);
	
	return bTag;
}


XNPTR XNDecompNode (int idx1, int idx2)   // bracketing index for matching tags

{
int idx1sub = idx1 + 1;
int idx2sub = idx2;
XNPTR pXN = 0;

//fprintf (stderr, "(XNDecompNode (%d %d)\n", idx1, idx2);



	if (XNIsATag (idx1))       // verify top of range is a start tag
	{
	char * backTag = XNBackTag (idx1);
	int boolBackTag = FALSE;
	int backTagIdx = -1;

	// search forward from idx1 for a matching back tag
		for (backTagIdx = idx1 + 1; backTagIdx <= idx2; backTagIdx ++)
		{
			if (! strcmp (backTag, itemList [backTagIdx]))
			{
				boolBackTag = TRUE;
				break;
			}
		}

		if (boolBackTag)
		{
				// ok, enough to create a node
			pXN = NewXNPtr ();
		
				// tag label
			pXN -> pXmlTag = TextMemory (itemList [idx1]);
			itemUsed [idx1] = TRUE;

//fprintf (stderr, "\tTag Pair (%d - %d)\n", idx1, backTagIdx);

				// does this tag have data
			if (idx1 + 1 < itemCount)
			{
				if (! XNIsATag (idx1 + 1))
				{
						// yes save it
					pXN -> pData = TextMemory (itemList [idx1 + 1]);
					itemUsed [idx1 + 1] = TRUE;
	
					idx1sub ++;
				}
			}

	// check for a child pair
			if (backTagIdx -1 >= idx1sub)
			{
//fprintf (stderr, "\tChild pair ? (%d %d)\n", idx1sub, backTagIdx -1);

				pXN -> pChild = XNDecompNode (idx1sub, backTagIdx -1);
			}

	// possible sibling pair
			if (idx2 > backTagIdx)		// something after the above tag pair?
			{
				if (idx2 - backTagIdx + 1 > 1)
				{
//fprintf (stderr, "\tSibling pair ? (%d %d)\n", backTagIdx + 1, idx2);

					pXN -> pNext = XNDecompNode (backTagIdx + 1, idx2sub);
				}
			}
		}
		else
		{
			sprintf (errBuf, "Err, backtag missing:'%s'", itemList [idx1]);
			Err (errBuf);
		}
	}
	else
	{
		sprintf (errBuf, "Err, expecting tag:'%s'", itemList[idx1]);
		Err (errBuf);
	}

	return (pXN);
}



XNDumpDepth (int d)

{

	while (d -- > 0)
		fprintf (stderr, "\t");
}

XNDumpTree (XNPTR xnp, int depth)

{

	if (xnp)
	{
		if (xnp -> pXmlTag)	
		{
			XNDumpDepth (depth);
			fprintf (stderr, "%s (%s)\n", xnp -> pXmlTag, xnp -> pData ? xnp -> pData : "_");
		}
	
		XNDumpTree (xnp -> pChild, depth + 1);
	
		XNDumpTree (xnp -> pNext, depth);
	
		fflush (stderr);
	}
}


void XNDecomp (char * rdat)

{
int i;



	// first, create a list of "items" either a <tag> or the text between tags

	for (; * rdat;)
	{
		if (* rdat == '<')
			rdat = TagItem (rdat);
		else
			rdat = TextItem (rdat);
	}

	if (debugI)
		for (i = 0; i < itemCount; i ++)
		{
			sprintf (errBuf, "%2d   %s\n", i, itemList [i]);
			Err (errBuf);
		}


	// Then create a linked list of the XML taggings, with the associated data

	pXNRoot = XNDecompNode (0, itemCount -1);

	if (debugI)
		fprintf (stderr, "%d mallocs\n", mallocsCount);

	for (i = 0; i < itemCount; i ++)
	{
		free (itemList [i]);
		mallocsCount --;
	}

	if (debugI)
		fprintf (stderr, "%d mallocs\n", mallocsCount);


	if (debugI)
	{
		XNDumpTree (pXNRoot, 0);
		fprintf (stderr, "\n");
		fflush (stderr);
	}
}


void XNPrep (int debug)

{
int idx;

	debugI = debug;

        for (idx = 0; idx < MAX_DATA_ITEMS; idx ++)
                itemUsed [idx] = FALSE;


        pXNRoot = 0;
}


XNPTR XNGetRoot ()

{

	return (pXNRoot);
}


void XNFreeMemory1 (XNPTR pXN)
	  
{

	if (pXN -> pXmlTag)
	{
		free (pXN -> pXmlTag);
		mallocsCount --;
	}

	if (pXN -> pData)
	{
		free (pXN -> pData);
		mallocsCount --;
	}

	if (pXN -> pChild)
		XNFreeMemory1 (pXN -> pChild);

	if (pXN -> pNext)
		XNFreeMemory1 (pXN -> pNext);

	free (pXN);
	mallocsCount --;
}


void XNFreeMemory ()

{

	if (pXNRoot)
		XNFreeMemory1 (pXNRoot);

	if (debugI)
		fprintf (stderr, "%d mallocs\n", mallocsCount);

	pXNRoot = 0;
	itemCount = 0;
	mallocsCount = 0;
}

#ifdef TESTONLY

//static char rdat [] = "<MSG><PLAY>1004.WAV</PLAY></MSG><PLAY>playtxt</PLAY>";
static char rdat [] = "<MSG><PLAY>1004.WAV</PLAY><WAITFOR><TIMEOUT>7</TIMEOUT><DIGITS>1</DIGITS></WAITFOR></MSG>";

int main(int argc, char* argv[])
{

	for (int i = 0; i < MAX_DATA_ITEMS; i ++)
		itemUsed [i] = FALSE;


	pXNRoot = 0;

	XNDecomp (rdat);


	XNFreeMemory ();
	fprintf (stderr, "%d mallocs\n", mallocsCount);

	getchar ();
	return 0;
}

#endif

