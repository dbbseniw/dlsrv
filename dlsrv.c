
/*   dlsrv.c    the dialogic "driver" for the touchtone interface


Still to be done:

	Command line option to allow the changing of the directory for the log and call log files





	1.3.0 - DLWaitRingCallerID () call from Main () now results in a sleep (1) if returns ERROR
		 to avoid 100% CPU churning;
	1.2.1c - Expanded xml limit to 20000 items to accomodate large directories
	1.2.1d - Added -v version only option to cmd line options
	1.2.2 - Removed unnecessary tracing
	1.2.3 - Removed unnecessary tracing
	1.3.0 - Corrected bug that recognized hangup during status list, but continued to detail statuses available
	1.4.0 - Record time limit moved from 1 minute to 3 minutes.
	2.0.0 - libdl modified to check for a .vox8 file first (8 khz sample rate), and use it if it exists.
		Failing that, to use the .vox file (the default/original 6 khz sample rate).   All recordings are now 
		made at the 8khz quality.
	
	2.1.0 - Adding Call forwarding, and option to DISABLE Caller ID   (-c)
	2.1.1 - Tracing added to locate possible Hang condition
*/



static char VER [] = "1.3.0";


#define ALLOCATE

#include <stdio.h>
#include "libdl.h"
#include "libxml.h"

#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <locks.h>

#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>


#define SUCCESS     1
#define ERROR       0

//#define CLIENT_IP "192.168.1.102"
#define CLIENT_PORT 6133

#define VOX_DIR  "/web/htdocs/vox/"

#define DEFAULT_DL_TIMEOUT         0
#define DEFAULT_DL_DIGITS         10
#define DEFAULT_DL_RESKEY          0

#define TXT1(a)   {fprintf (stderr, a); fflush (stderr);}
#define TXT(a)   {fprintf (stderr, a); fflush (stderr);}
#define TXT2(a,b)   {fprintf (stderr, a, b); fflush (stderr);}
#define TXT3(a,b,c)   {fprintf (stderr, a, b,c); fflush (stderr);}
#define TXT4(a,b,c,d)   {fprintf (stderr, a, b,c,d); fflush (stderr);}

#define DTXT1(a)   {if (pgm.debugI) TXT1(a)}
#define DTXT(a)   {if (pgm.debugI) TXT(a)}
#define DTXT2(a,b)   {if (pgm.debugI) TXT2(a,b)}
#define DTXT3(a,b,c)   {if (pgm.debugI) TXT3(a,b,c)}
#define DTXT4(a,b,c,d)   {if (pgm.debugI) TXT4(a,b,c,d)}


#define PHONE_LOG_FN "/tmp/PhoneLog%s"      // log file for 1 call
#define CALLS_LOG_FN "/var/log/Calls.log"   // accumulation of above files catted to this file
#define CALLER_HUP_LOCK_TEXT	"Caller has Hung Up"


static char LockText_C [100];


typedef struct PGM_INFO
{
   char * pgm;
   int debugI;
   int bTerminateCall;
   int bTerminatePgm;
   int phoneOffhookI;
   int listenSocketHandle;
   int rxtxSocketHandle;
   struct sockaddr_in servAddr;
   struct sockaddr_in cliAddr;

   char sIP [50];
   int bIP;

   int iPort;
   int bPort;

   char sDialogicLine [50];
   int bDialogicLine;

   int bHangupAndExit;
   int bVersionAndExit;
   int bDisableCallerID;

   char sPhoneLog [300];   
   FILE * fPhoneLog;
} PGM, * PGMPTR;

PGM pgm;




#define CALLER_ID_TXT_LEN  100

typedef struct caller_id_info
{
   char pCallingNumber [CALLER_ID_TXT_LEN + 1];
   int  bCallingNumberValid;
   char pDialedNumber [CALLER_ID_TXT_LEN + 1];
   int  bDialedNumberValid;
} CALLID, * CALLIDPTR;

CALLID callIdInfo;




#define MIN_SOCKET_READ_LEN 100

int m_rxBufChars = 0, m_rxBufLen = 0;
char * m_pRxBuf = 0;

int m_txBufChars = 0, m_txBufLen = 0;
char * m_pTxBuf = 0;


FileExists(fname,notify)
char *fname;
int notify;
{   
struct stat buf;   
char ebuf[80], *StripPath();
int ret;     

   if((stat(fname,&buf)) == 0)              /* file found ok */  
       ret = SUCCESS;
   else
      ret = ERROR;
   return(ret);
}


int SocketTxData (int socketHandle, char * dat, int len)

{
int ret, sendableBytes;
int SocketTxRet = 0;

int ttl = m_txBufChars + len;

//DSDS
{
char trace [1000], * pt;

	strcpy (trace, dat);
	pt = strchr (trace, '\n');
   	if (pt)
		* pt = 'N';
	
	pt = strchr (trace, '\r');
   	if (pt)
		* pt = 'R';


//TXT2 ("\nSENDING: '%s'\n", trace);
}
	
	if (ttl > m_txBufLen)
	{
	char * tmp = (char *) malloc (ttl);

		if (tmp)
		{
			if (m_txBufChars)
				memcpy (tmp, m_pTxBuf, m_txBufChars);

			if (m_pTxBuf)
				free (m_pTxBuf);

			m_pTxBuf = tmp;
			m_txBufLen = ttl;
		}
	}
	// else tx buffer already large enough

	memcpy (m_pTxBuf + m_txBufChars, dat, len);
	m_txBufChars += len;

	errno = 0;

	for (;;)
	{
		ret = send (socketHandle, m_pTxBuf, m_txBufChars, MSG_NOSIGNAL); 

		//DTXT2 ("send () ret %d chars send\n", ret);
	
		if (! ret && errno == EINTR)	// call interrupted, try again
		{
			TXT1 ("I");
		}
		else
			break;			// normal return
	}

        if (pgm.debugI)
		if (! ret)
			TXT2 ("SocketTxData Error: %s\n", strerror(errno));

//        if (pgm.debugI) 
//	   TXT3 ("SocketTx (%d chars), %d sent)\n", m_txBufChars, ret);

	if (ret)
	{
		m_txBufChars -= ret;

		//printf ("Receive () = %d chars\n", ret);

//		m_txBufChars -= DeliverTxPacket (m_pTxBuf, m_txBufChars);
	}

	return (SocketTxRet);
}


int SocketTxString (int socketHandle, char * str)

{

   return SocketTxData (socketHandle, str, strlen (str));
}


char * SocketRxBuffer ()

{

	return m_pRxBuf;
}


int SocketRx (int socketHandle)

{
int ret, readableBytes, deliverRet;
int SocketRxRet = 0;
	
	int ttl = m_rxBufChars + MIN_SOCKET_READ_LEN;

	if (ttl > m_rxBufLen)
	{
	char * tmp = (char *) malloc (ttl);

		if (tmp)
		{
			if (m_rxBufChars)
				memcpy (tmp, m_pRxBuf, m_rxBufChars);

			if (m_pRxBuf)
				free (m_pRxBuf);

			m_pRxBuf = tmp;
			m_rxBufLen = ttl;
		}
	}

	ret = read (socketHandle, m_pRxBuf + m_rxBufChars, 1); 

	if (ret)
	{
		//DTXT2 ("read () == %d\n", ret);
	}
	else
	{
		// DTXT ("read () == 0\n");
	}

	if (ret)
	{
		if (* m_pRxBuf == '\n' || * m_pRxBuf == '\r')
		{
			if (m_rxBufChars)
			{
				* m_pRxBuf = '\n';
				m_rxBufChars += 1;	
			}
			//  ELSE ignore unpreceded CR or LF
		}
		else
		{
			m_rxBufChars += 1;
		}

		for (;;)
		{
			//DTXT3 ("  DeliverRxPacket () = %d chars,  %d,,,,\n", ret, m_rxBufChars);

			deliverRet = DeliverRxPacket (m_pRxBuf, m_rxBufChars);
			//DTXT2 ("                       returns %d\n", deliverRet);

     			if (deliverRet) 	// some of the data used
			{
				if (m_rxBufChars > deliverRet)// still some data off the end
				{
					memmove (m_pRxBuf, m_pRxBuf + deliverRet, m_rxBufChars - deliverRet);
					m_rxBufChars -= deliverRet; 
					* (m_pRxBuf + m_rxBufChars) = 0;
				}
				else
					m_rxBufChars = 0;
			}
			else
			{
				//DTXT ("   DeliverRxPacket () ret == 0\n");
				break;
			}
	
			* (m_pRxBuf + m_rxBufChars) = 0;
	
			//DTXT3 ("Post DeliverRxPacket (), remains = '%s' (%d chars)\n", m_pRxBuf, m_rxBufChars);

			SocketRxRet = m_rxBufChars;

			//printf ("SocketRx (), back from DeliverRxPacket () (ret %d), %d left in buffer\n", deliverRet, m_rxBufChars);
		}
	}
	else
	{
		TXT4 ("read () ret = %d, errno = %d, %s\n", ret, errno, strerror (errno));
		// sleep (1);  // 
		SocketRxRet = -1;
	}

	return (SocketRxRet);
}




int ListenSocketInit ()
{
int ret;
int optVal = 1;

  /* create socket */
  pgm.listenSocketHandle = socket(AF_INET, SOCK_STREAM, 0);

  if(pgm.listenSocketHandle<0)
  {
    TXT2 ("Socket Init Error: %s\n", strerror(errno));
    return ERROR;
  }
 
  /* bind server port */
  pgm.servAddr.sin_family = AF_INET;
  pgm.servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  pgm.servAddr.sin_port = htons(CLIENT_PORT);

  // allow for reuse of the socket immediately after pgm terminates, 
  // not waiting for the system to recycle the port (about a minute)

  ret = setsockopt (pgm.listenSocketHandle, SOL_SOCKET, SO_REUSEADDR, & optVal, sizeof (int));
  
  if (ret)
  {
    TXT2 ("Setsocopt () Fail: %s\n", strerror (errno));
    return (ERROR);
  }

  if(bind(pgm.listenSocketHandle, (struct sockaddr *) &pgm.servAddr, sizeof(pgm.servAddr))<0)
  {
    TXT2 ("Socket Bind Error: %s\n", strerror(errno));
    return ERROR;
  }

  if (listen (pgm.listenSocketHandle,0))
  {
    TXT2 ("Socket Listen Error: %s\n", strerror(errno));
    return ERROR;
  }

   TXT1 ("TCPIP Socket Initialized\n");

   return SUCCESS;
}


DisconnectSocket ()

{
	
	DTXT ("Closing TCP Socket\n");

    	close (pgm.rxtxSocketHandle);

	pgm.rxtxSocketHandle = 0;
}

	// create a socket and connect out to a listening process
int ConnectSocketInit ()

{
int ret;

  /* create socket */
  pgm.rxtxSocketHandle = socket(AF_INET, SOCK_STREAM, 0);

  if(pgm.rxtxSocketHandle < 0)
  {
    TXT2 ("Socket Init Error: %s\n", strerror(errno));
    return ERROR;
  }
  if (pgm.debugI)
     TXT1 ("Socket () Successful\n");

  pgm.cliAddr.sin_family = AF_INET;
  if (! inet_aton (pgm.sIP, & pgm.cliAddr.sin_addr) && strcmp (pgm.sIP, "localhost"))
  {
     TXT2("Invalid IP address: %s\n", pgm.sIP);
  }

  if (pgm.debugI)
     TXT2 ("Using machine: %s\n", pgm.sIP);

  pgm.cliAddr.sin_port = htons (pgm.iPort);

  if (pgm.debugI)
     TXT3 ("\tConnectSocketInit (), Pre connect to touchtone (%s %d)\n", pgm.sIP, pgm.iPort);

  ret = connect (pgm.rxtxSocketHandle, (struct sockaddr *) (& pgm.cliAddr), sizeof (pgm.cliAddr));
  
  if(ret)
  {
    TXT4 ("Socket Init Connect Error: %s, (%s %d)\n", strerror(errno), pgm.sIP, pgm.iPort);
    return ERROR;
  }

  if (pgm.debugI)
     TXT1 ("Connect () to touchtone Successful\n");

  return SUCCESS;
}


int SocketSend (char * txt)

{
char bufr [10000];

   strcpy (bufr, txt);

   if (txt [strlen (txt) -1] != '\n')
      strcat (bufr, "\n");

   TXT2("SocketSend (%s)\n", bufr);

   SocketTxString (pgm.rxtxSocketHandle, bufr);
}



char * XNCmd (XNPTR pXN)

{

	if (pXN)
		if (pXN -> pXmlTag)
			return (pXN -> pXmlTag);

	return "<ERROR>";
}


#define TAG_MSG         "<MSG>"
#define TAG_CALLRX      "<CALLRX>"
#define TAG_CALLID      "<CALLID>"
#define TAG_LINE        "<LINE>"
#define TAG_DATETIME    "<DATETIME>"

#define TAG_PLAY        "<PLAY>"
#define TAG_RECORD      "<RECORD>"
#define TAG_POSTPLAY    "<POSTPLAY>"
#define TAG_RESPONSE    "<RESPONSE>"
#define TAG_STATE	"<STATE>"
#define TAG_FORWARD	"<FORWARD>"
#define TAG_TRACEMSG    "<TRACEMSG>"		// text to be placed in the dialogic lock file
#define TAG_LOGMSG      "<LOGMSG>" 		// text to be sent to the phone call log file

#define TAG_ACTION	"<ACTION>"
#define ACT_IGNRINPUT 	"IGNRINPUT"
#define ACT_HANGUP	"HANGUP"
#define ACT_TIMEOUT    	"TIMEOUT"

#define TAG_WAITFOR	"<WAITFOR>"
#define TAG_TIMEOUT	"<TIMEOUT>"     // seconds to wait between key presses after the play
#define TAG_DIGITS	"<DIGITS>"
#define TAG_NOWAIT      "<NOWAIT>"      // if NOWAIT is specified, do not respect the TIMEOUT
					// value after the play (do not wait for any response) unless the user has terminated
					// the play with a key (and then we wait for the user to finish the entry, respecting
					// the TIMEOUT value)

#define TAG_RESKEY	"<RESKEY>"
#define TAG_	"<>"


#define ISCMD(a)      (! strcmp (XNCmd (pXN), a))
#define IFISCMD(a)    if (ISCMD(a))

#define CALL_TERMINATION_MSG	"<MSG><ACTION>HUNGUP</ACTION></MSG>\n"



char * BackTag (char * tag)

{
static char bTag [50];
char * dat = bTag;

	* (dat ++) = * tag;
	* (dat ++) = '/';
	strcpy (dat, tag + 1);

	return (bTag);
}


#define ACTION_TXT_LEN        100
#define ACTION_PRIMARY_PLAY     1
#define ACTION_PRIMARY_RECORD   2
#define ACTION_PRIMARY_HANG_UP  3
#define ACTION_PRIMARY_FORWARD  4

typedef struct ACTION_STRUCT
{
   int primaryAction;

   char * pPlayVox;
   char * pRecordVox;
   char * pForward;

   char * pPostPlay;
   int bPostPlay;

   char * pState;
   int bState;

   char * pAction;
   int  bAction;

   int  bIgnrInput;

   char * pTimeout;
   int bTimeout;

   int bNoWait;

   int iDigits;
   int bDigits;

   char * pReskeys;
   int bReskeys;

   char sKeyPresses [ACTION_TXT_LEN + 1];
   int iKeyPressesKeys;

//  int bCmdFinished; // BOOL, if TRUE, skip playing remainder of PLAY cmds.
//      This is now a separate BOOL as this struct is flushed after each play

   int dlTermCode;   // DL Termintion Code

   int bErr;
   char errTxt [ACTION_TXT_LEN + 1];
} ACTION, * ACTIONPTR;

int bCmdFinished; // BOOL, if TRUE, skip playing remainder of PLAY cmds.

 
int ExecuteFilledActionStruct (ACTIONPTR pAct, int fillRet)

{

	//DTXT3 ("IN ExecuteFilledActionStruct (%d, %d)\n", pAct -> primaryAction, fillRet);

	if (! fillRet)
	{
		TXT1 ("Error in FillActionStruct ()\n");
		return ERROR;
	}


	if (bCmdFinished)
	{
		DTXT ("bCmdFinished == TRUE, ExecuteFilledActionStruct () terminating before PLAY\n");
		return SUCCESS;
	}

	switch (pAct -> primaryAction)
	{
		case ACTION_PRIMARY_PLAY:
			CmdPlay (pAct);
			break;

		case ACTION_PRIMARY_RECORD:
			CmdRecord (pAct);
			break;

		case ACTION_PRIMARY_FORWARD:
			CmdForward (pAct);
			break;

		case ACTION_PRIMARY_HANG_UP:
			if (pgm.debugI)
				TXT ("Hanging up on request of Client (TCP Command)\n");

			DLSrvHangUp ();
			break;

		default:
			// nix
	}
}


void InitActionStruct (ACTIONPTR pAct, int resetCmdFinished)

{

	memset (pAct, 0, sizeof (ACTION));
	
	if (resetCmdFinished)	
		bCmdFinished = FALSE; 
}


void LogFileText (char * txt)

{

	DLLockFileText (txt, pgm.phoneOffhookI);
}



char * ElapsedTime (int bStart, char * buf)

{
static time_t startTime;
int elapsed;
int iSec, iMin;

	if (bStart)
	{
		startTime = time(0);
		return "";
	}

	elapsed = time(0) - startTime;

	iMin = elapsed / 60;
	iSec = elapsed % 60;

	sprintf (buf, "%02d:%02d", iMin, iSec);
	return buf;	
}


char * SQLTimeDate (char * buf)

{
time_t ti;
struct tm * t;

   	ti = time (0);
      	t = localtime (& ti);      

	sprintf (buf, "%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d",
		t -> tm_year + 1900, t -> tm_mon + 1, t -> tm_mday ,
		t -> tm_hour, t -> tm_min, t -> tm_sec);

	return (buf);
}


void ClosePhoneLog ()

{
char datetime [100];
char elapsed [10];
char cmd [1000];

	if (pgm.fPhoneLog)
	{
		fprintf (pgm.fPhoneLog, "</LOGTEXT>\n");
		fprintf (pgm.fPhoneLog, "<ELAPSED>%s</ELAPSED>\n", ElapsedTime (FALSE, elapsed));
		fprintf (pgm.fPhoneLog, "<ENDTIME>%s</ENDTIME>\n", SQLTimeDate (datetime));
		fprintf (pgm.fPhoneLog, "</CALL>\n\n");
		fflush (pgm.fPhoneLog);
		fclose (pgm.fPhoneLog);
	
		sprintf (cmd, "cat %s >> %s", pgm.sPhoneLog, CALLS_LOG_FN);
		system (cmd);

		pgm.fPhoneLog = 0;	

		sprintf (cmd, "rm %s", pgm.sPhoneLog);
		system (cmd);
	}
}


int DLLocksActive ()

{
char buf [101];
int board, line, active = 1;

	for (board = 1; board < 5; board ++)
	{
		for (line = 1; line < 9; line ++)
		{
			if (DLLockFileTextNOLock (board, line, buf))
			{
				if (* buf == '1')
					active ++;
			}
		}
	}

	return (active);
}


void PhoneLogText (char * txt)

{
char datetime [100];
char * dat;

	if (! pgm.fPhoneLog)
	{
	char datetime [100];

		sprintf (pgm.sPhoneLog, PHONE_LOG_FN, pgm.sDialogicLine);

		pgm.fPhoneLog = fopen (pgm.sPhoneLog, "w"); 
		if (pgm.fPhoneLog)
		{
			ElapsedTime (TRUE, 0); // start the elapsed time timer

			fprintf (pgm.fPhoneLog, "<CALL>\n");
			fprintf (pgm.fPhoneLog, "<STARTTIME>%s</STARTTIME>\n", SQLTimeDate (datetime));
			fprintf (pgm.fPhoneLog, "<ACTIVELINES>%d</ACTIVELINES>\n", DLLocksActive ());

			if (strlen (pgm.sDialogicLine) > 4)
				dat = pgm.sDialogicLine +4;
			else
				dat = pgm.sDialogicLine;
			fprintf (pgm.fPhoneLog, "<PORT>%s</PORT>\n", dat);

			if (callIdInfo.bCallingNumberValid)
				dat = callIdInfo.pCallingNumber;
			else
				dat = "";
			fprintf (pgm.fPhoneLog, "<CALLERID>%s</CALLERID>\n", dat);

			if (callIdInfo.bDialedNumberValid)
				dat = callIdInfo.pDialedNumber;
			else
				dat = "";
			fprintf (pgm.fPhoneLog, "<CALLEDID>%s</CALLEDID>\n", dat);


			fprintf (pgm.fPhoneLog, "<LOGTEXT>\n");
		}
	}

	if (pgm.fPhoneLog)
	{
		fprintf (pgm.fPhoneLog, "%s\n", txt);
		fflush (pgm.fPhoneLog);
	}
}


/*
<MSG><PLAY>962.vox</PLAY><POSTPLAY>!</POSTPLAY><STATE>IGNORE</STATE><ACTION>IGNRINPUT</ACTION></MSG>


<MSG>
<PLAY>962.vox</PLAY><POSTPLAY>!</POSTPLAY><STATE>IGNORE</STATE><ACTION>IGNRINPUT</ACTION>
<TRACEMSG>Playing directory of statuses</TRACEMSG>
<PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>901.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>sta1.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>902.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>sta2.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>903.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>sta3.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>910.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>sta10.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR>
<PLAY>945.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>*#</RESKEY></WAITFOR>
<TRACEMSG>Waiting for status</TRACEMSG>
</MSG>


<MSG><PLAY>962.vox</PLAY><POSTPLAY>!</POSTPLAY><STATE>IGNORE</STATE><ACTION>IGNRINPUT</ACTION><TRACEMSG>Playing directory of statuses</TRACEMSG><PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>901.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>sta1.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>902.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>sta2.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>903.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>sta3.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>955.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>910.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>sta10.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>pause.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>#</RESKEY><NOWAIT>1</NOWAIT></WAITFOR><PLAY>945.vox</PLAY><STATE>NEWSTATUS2</STATE><WAITFOR><TIMEOUT>10</TIMEOUT><RESKEY>*#</RESKEY></WAITFOR><TRACEMSG>Waiting for status</TRACEMSG></MSG>

*/


int FillActionStructAndExecute (XNPTR pXN, ACTIONPTR pAct, int bLevel)

{
int ret = SUCCESS;
int executed = FALSE;

//DTXT3 ("FillActionStructAndExecute(level= %d, bTerminate= %d)\n", bLevel, pgm.bTerminateCall);

	if (pgm.bTerminateCall)
			// short circuit the play
		return FALSE;

	if (bLevel == 0)
  		InitAction (pAct);

	IFISCMD (TAG_MSG)
	{
                //TXT ("FillActionStruct (),   MSG tag ignored\n");
	}
	else IFISCMD (TAG_PLAY)
	{
//DTXT2 ("Fill... () <PLAY> (%s) found\n", pXN -> pData);

		if (pAct -> primaryAction)   // Found second play cmd 
		{
//DTXT2 ("Fill... () <PLAY> previous PLAY exists (%s)\n", pAct -> pPlayVox);
//DTXT3 ("Fill... ()           executed = %d,    bLevel = %d\n", executed, bLevel);
			//if (! executed && bLevel == 0 )          <=============================================    ?
			if (! executed) // found a second play, and first is not been executed...
			{
				ExecuteFilledActionStruct (pAct, ret);
				InitActionStruct (pAct, FALSE);
			}
		}

 		pAct -> pPlayVox = pXN -> pData;
                pAct -> primaryAction = ACTION_PRIMARY_PLAY;
	} 
	else IFISCMD (TAG_RECORD)
	{
 		pAct -> pRecordVox = pXN -> pData;
                pAct -> primaryAction = ACTION_PRIMARY_RECORD;
	}
	else IFISCMD (TAG_POSTPLAY)
	{
		pAct -> pPostPlay = pXN -> pData; 
		pAct -> bPostPlay = TRUE;
	}
	else IFISCMD (TAG_FORWARD)
	{
		pAct -> pForward = pXN -> pData;
		pAct -> primaryAction = ACTION_PRIMARY_FORWARD;
	}
	else IFISCMD (TAG_STATE)
	{
		pAct -> pState = pXN -> pData; 
		pAct -> bState = TRUE;
	}
	else IFISCMD (TAG_ACTION)
	{
		pAct -> pAction = pXN -> pData; 
		pAct -> bAction = TRUE;

		if (! strcmp (pXN -> pData, ACT_IGNRINPUT))
			pAct -> bIgnrInput = TRUE;

		if (! strcmp (pXN -> pData, ACT_HANGUP))
                	pAct -> primaryAction = ACTION_PRIMARY_HANG_UP;
	}
	else IFISCMD (TAG_TIMEOUT)
	{
		pAct -> pTimeout = pXN -> pData; 
		pAct -> bTimeout = TRUE;
	}
	else IFISCMD (TAG_NOWAIT)
	{
		pAct -> bNoWait = TRUE;
	}
	else IFISCMD (TAG_DIGITS)
	{
		pAct -> iDigits = atoi (pXN -> pData); 
		pAct -> bDigits = TRUE;
	}
	else IFISCMD (TAG_RESKEY)
	{
		pAct -> pReskeys = pXN -> pData; 
		pAct -> bReskeys = TRUE;
	}	
	else IFISCMD (TAG_TRACEMSG)
	{
		if (pXN -> pData)
		{
			DTXT2 ("LogFileText (%s) (Lock File Text)\n", pXN -> pData);
			LogFileText (pXN -> pData);
		}
	}
	else IFISCMD (TAG_LOGMSG)
	{
		if (pXN -> pData)
		{
			DTXT2 ("PhoneLogText (%s)\n", pXN -> pData);
			PhoneLogText (pXN -> pData);
		}
	}	


	if (ret)
		if (pXN -> pChild)
		{
//			DTXT3 ("	Calling Fill... for Child (%d -> %d)\n", bLevel, bLevel + 1);
			ret = FillActionStructAndExecute (pXN -> pChild, pAct, bLevel + 1);
		}

	if (ret)
		if (pXN -> pNext)
		{
//			DTXT3 ("	Calling Fill... for Adjacent (%d -> %d)\n", bLevel, bLevel + 1);
			ret = FillActionStructAndExecute (pXN -> pNext, pAct, bLevel);
		}

	if (! executed && bLevel == 0 )
	{
		ExecuteFilledActionStruct (pAct, ret);
		InitActionStruct (pAct, FALSE);
	}

//DTXT2  ("FillActionStruct () returning %d\n", ret);

	return ret;
}


InitAction (ACTIONPTR pAct)

{

   memset ((char *) pAct, 0, sizeof (ACTION));
}


	// encode the "return" key for Dialogic
int DlFormResKey (char key)

{

	switch (key)
	{
		case '0':
			return (DM_0);
		case '1':
			return (DM_1);
		case '2':
			return (DM_2);
		case '3':
			return (DM_3);
		case '4':
			return (DM_4);
		case '5':
			return (DM_5);
		case '6':
			return (DM_6);
		case '7':
			return (DM_7);
		case '8':
			return (DM_8);
		case '9':
			return (DM_9);
		case '*':
			return (DM_S);
		case '#':
			return (DM_P);
	}

	return (0);
}


int DlFormResStr (ACTIONPTR pAct)

{
char * pt;
int val = 0;
	
	if (pAct -> pReskeys)
	{
		for (pt = pAct -> pReskeys; * pt; pt ++)
			if (* pt != ' ' && * pt != ',')
				val += DlFormResKey (* pt);
	}

	return val;
}


int RequiredKeyPressed (char * pressedKeys, char * returnKeys)

{
char * pt;
	
	for (;* returnKeys; returnKeys ++)
		for (pt = pressedKeys; * pt; pt ++)
			if (* pt == * returnKeys)
				return TRUE;

	return FALSE;
}


void CmdAnswer ()

{

	if (! pgm.phoneOffhookI)   // yet to be answered
	{
		if (! DLAnswerRingingCall ())
		{
			TXT ("Error, in answering Ringing Call\n");
		}
		else
		{
			pgm.phoneOffhookI = TRUE;

			if (pgm.debugI)
				TXT ("Call Answered\n");
		}
  	}
  	else
	{
		//DTXT ("Already Off Hook\n");
	}
}


int DlActionTerminatedByTimeout (ACTIONPTR pAct)

{

	return pAct -> dlTermCode == TM_IDDTIME;	// play/record terminated by timeout on callers part
}


int CallTerminatedByHangup (ACTIONPTR pAct)

{
int ret;

	ret = pAct -> dlTermCode == TM_LCOFF;	// call terminated by caller

	if (ret)
	{
		LogFileText (CALLER_HUP_LOCK_TEXT);
		PhoneLogText (CALLER_HUP_LOCK_TEXT);
	}

	return ret;
}



#define CLR(a)  memset (a, 0, sizeof (a));

void CmdPlayResponse (ACTIONPTR pAct)

{
char buf [200];
char state [100];
char postplay [100];
char response [100];
char termcode [100];

	CLR (buf);
	CLR (state);
	CLR (postplay);
	CLR (response);
	CLR (termcode);

	if (pAct -> bPostPlay || pAct -> bState || pAct -> iKeyPressesKeys)
	{

		if (pAct -> bPostPlay)
			sprintf (postplay, "%s%s%s", TAG_RESPONSE, pAct -> pPostPlay, BackTag (TAG_RESPONSE));

		if (pAct -> bState)
			sprintf (state, "%s%s%s", TAG_STATE, pAct -> pState, BackTag (TAG_STATE));

		if (DlActionTerminatedByTimeout (pAct))	    // if timeout
			sprintf (response, "%s%s%s", TAG_RESPONSE, ACT_TIMEOUT, BackTag (TAG_RESPONSE));
		else
			if (pAct -> iKeyPressesKeys)
				sprintf (response, "%s%s%s", TAG_RESPONSE, pAct -> sKeyPresses, BackTag (TAG_RESPONSE));

		DTXT3 ("\t\t\t\t\t\t\t\t\t\t_%s_ (%d keys)\n", pAct -> sKeyPresses, pAct -> iKeyPressesKeys);

   		sprintf (buf, "<MSG>%s%s%s</MSG>", state, postplay, response);

   		SocketSend (buf);
	}
}


char * FullVoxPath (char * fn)

{
static char full [512];

	strcpy (full, VOX_DIR);
	strcat (full, fn);

	return (full);
}


void CmdRecordResponse (ACTIONPTR pAct)

{
char buf [200];
char state [100];
char response [100];
char termcode [100];

        if (pAct -> bState)
                sprintf (state, "%s%s%s", TAG_STATE, pAct -> pState, BackTag (TAG_STATE));
        else
                * state = 0;

	if (pAct -> iKeyPressesKeys)
		if (DlActionTerminatedByTimeout (pAct))
			sprintf (response, "%s%s%s", TAG_RESPONSE, ACT_TIMEOUT, BackTag (TAG_RESPONSE));
		else
			sprintf (response, "%s%s%s", TAG_RESPONSE, pAct -> sKeyPresses, BackTag (TAG_RESPONSE));
	else
                       * response = 0;

        sprintf (buf, "<MSG>%s%s</MSG>", state, response);

        SocketSend (buf);
}


int CmdRecord (ACTIONPTR pAct)

{
int recRet;

	CmdAnswer ();   // answer the phone if necessary

	if (pgm.phoneOffhookI)
 	{
		recRet = DLRecord (FullVoxPath (pAct -> pRecordVox), pAct -> bTimeout ? atoi (pAct -> pTimeout) : 600, 
				pAct -> bReskeys ? DlFormResStr (pAct) : DEFAULT_DL_RESKEY);

		pAct -> dlTermCode = DLReturnTermCode ();

     		if (CallTerminatedByHangup (pAct))	// call terminated by caller
		{
			DTXT2 ("DLRecord () ret %d  (call terminated)\n", recRet);
			pgm.bTerminateCall = TRUE;

			return;
		}

		strcpy (pAct -> sKeyPresses, DLGetKeyString ());
		pAct -> iKeyPressesKeys = strlen (pAct -> sKeyPresses);

		DTXT3 ("Post DLRecord, %d Keys = '%s' pressed\n", pAct -> iKeyPressesKeys, pAct -> sKeyPresses);
		
		CmdPlayResponse (pAct);
	}

	return SUCCESS;
}


int CmdForward (ACTIONPTR pAct)
{
	DTXT2 ("FORWARDing call: '%s'\n", pAct -> pForward);

	DLForward (pAct -> pForward);
	return SUCCESS;
}


int CmdPlay (ACTIONPTR pAct)

{
int ret;
int playRet;
int offHook;
int bContinueWaiting = TRUE;   // after the play, continue waiting for more keypresses
int bResponse = TRUE;

//printf ("TRACE, PLAY action text = '%s'\n", pAct -> pPlayVox);

	CmdAnswer ();   // answer the phone if necessary


#ifdef MSG_TEST_MSGTEXT

<MSG><PLAY>test.vox</PLAY><POSTPLAY>!</POSTPLAY><STATE>LOGIN</STATE><ACTION>IGNRINPUT</ACTION></MSG>
<MSG><PLAY>test.vox</PLAY><ACTION>IGNRINPUT</ACTION></MSG>
<MSG><PLAY>test.vox</PLAY><ACTION>IGNRINPUT</ACTION><WAITFOR><DIGITS>4</DIGITS></WAITFOR></MSG>
<MSG><ACTION>HANGUP</ACTION></MSG>
<MSG><PLAY>test.vox</PLAY></MSG>
<MSG><PLAY>OffhookTest.vox</PLAY></MSG>
<MSG><PLAY>20ms.vox</PLAY></MSG>

<MSG><PLAY>test.vox</PLAY><WAITFOR><DIGITS>4</DIGITS></WAITFOR></MSG>
<MSG><PLAY>test.vox</PLAY><WAITFOR><DIGITS>4</DIGITS><RESKEY>#</RESKEY></WAITFOR></MSG>
<MSG><PLAY>test.vox</PLAY><WAITFOR><RESKEY>#</RESKEY></WAITFOR></MSG>
<MSG><PLAY>test.vox</PLAY><WAITFOR><RESKEY>#</RESKEY><TIMEOUT>5</TIMEOUT></WAITFOR></MSG>
<MSG><PLAY>test.vox</PLAY><WAITFOR><RESKEY>1,2,3</RESKEY><DIGITS>4</DIGITS><TIMEOUT>10</TIMEOUT></WAITFOR><POSTPLAY>!</POSTPLAY><STATE>Just text indicating State</STATE></MSG>

<MSG><ACTION>HANGUP</ACTION></MSG>

<MSG><RECORD>rectest.vox</RECORD></MSG>

<MSG><PLAY>937.vox</PLAY><ACTION>IGNRINPUT</ACTION></MSG>
<MSG><ACTION>HANGUP</ACTION></MSG>


<MSG><PLAY>901.vox</PLAY><PLAY>902.vox</PLAY><PLAY>903.vox</PLAY><PLAY>904.vox</PLAY></MSG>

<MSG><PLAY>937.vox</PLAY><ACTION>IGNRINPUT</ACTION></MSG>
<MSG><LOGMSG>text</LOGMSG></MSG>
<MSG><FORWARD>&1234</FORWARD></MSG>

#endif


if (pgm.phoneOffhookI)
{
	DLClearKeyBuffer ();

	//DTXT3 ("Pre DLPlay(), Keys so far = '%s', and '%s'\n", pAct -> sKeyPresses, DLGetKeyString ());

       		playRet = DLPlayExt (FullVoxPath (pAct -> pPlayVox), 60,
			1,
//			pAct -> bDigits ? pAct -> iDigits :
//				DEFAULT_DL_DIGITS, 
			pAct -> bReskeys ? DlFormResStr (pAct) : DEFAULT_DL_RESKEY,
 			pAct -> bIgnrInput);

		pAct -> dlTermCode = DLReturnTermCode ();

     		if (CallTerminatedByHangup (pAct))	// call terminated by caller
		{
			DTXT2 ("DLPlayExt () ret %d  (call terminated)\n", playRet);
			pgm.bTerminateCall = TRUE;

			return;
		}

			// responses after the DLPlayExt ()

		if (pAct -> bIgnrInput)
		{
			DLClearKeyBuffer ();
		}
		else
		{
// following two uncommented to enable the typeahead during the play portion.

			strcpy (pAct -> sKeyPresses, DLGetKeyString ());
			pAct -> iKeyPressesKeys = strlen (pAct -> sKeyPresses);
			
			DLClearKeyBuffer ();
		}

		if (pgm.debugI)
 		{
			if (pAct -> iKeyPressesKeys)
			{
				//DTXT3 ("Post Play, %d Keys (so far)  = '%s'\n", strlen (DLGetKeyString ()), DLGetKeyString ());
				DTXT3 ("Post Play, %d Keys (so far)  = '%s'\n", pAct -> iKeyPressesKeys, pAct -> sKeyPresses);
			}
			else
				DTXT ("Post Play, No Key Presses during Play\n");
		}


		offHook = DLIsOffHook ();
		// DTXT2 ("Phone is %s\n", offHook ? "OFF Hook" : "On Hook");


		// DTXT2 ("Checking to decide on calling DLGetDigitsExt (), DLPlayExt () returns '%s'\n", pAct -> sKeyPresses);

		if (pAct -> bReskeys)
			if (RequiredKeyPressed (pAct -> sKeyPresses, pAct -> pReskeys))
			{
				DTXT2 ("Post Play (), Required key '%s' FOUND in Play (), return keys\n", pAct -> pReskeys);
				bContinueWaiting = FALSE;
			}

 		if (bContinueWaiting)
			if (pAct -> bDigits)
				if (pAct -> iKeyPressesKeys >=  pAct -> iDigits)
				{
					DTXT2 ("Post Play (), Max Digits (%d) collected in Play ()\n", pAct -> iKeyPressesKeys);
					bContinueWaiting = FALSE;
				}

		if (bContinueWaiting)
			if (! pAct -> bTimeout)
				bContinueWaiting = FALSE;
		
		if (bContinueWaiting)
			if (pAct -> bTimeout)
				if (pAct -> pTimeout)
					if (! atoi (pAct -> pTimeout))
						bContinueWaiting = FALSE;

		if (bContinueWaiting)
			if (pAct -> bNoWait)
				if (pAct -> iKeyPressesKeys == 0)	// no key (s) pressed during play
					bContinueWaiting = FALSE;

			// if still waiting for full response
		if (bContinueWaiting)
		{
			DTXT2 ("Post DLPlay(), Continuing to wait, Keys so far = '%s'  GetDigitsExt () called\n", pAct -> sKeyPresses);

			playRet = DLGetDigitsExt (pAct -> bTimeout ? atoi (pAct -> pTimeout) : DEFAULT_DL_TIMEOUT, 
				pAct -> bDigits ? 
					pAct -> iDigits - pAct -> iKeyPressesKeys : 
					DEFAULT_DL_DIGITS,
				pAct -> bReskeys ? DlFormResStr (pAct) : DEFAULT_DL_RESKEY);
		
			pAct -> dlTermCode = DLReturnTermCode ();

			//TXT2 ("DLGetDigitsExt () ret %d\n", playRet);

     			if (CallTerminatedByHangup (pAct))	// call terminated by caller
			{
				DTXT2 ("DLGetDigitsExt () ret %d  (-1 == call terminated)\n", playRet);
				pgm.bTerminateCall = TRUE;
	
				return;
			}

			DTXT3 ("Post GetDigits() %d Keys during GetDigits()  = '%s'\n", strlen (DLGetKeyString ()), DLGetKeyString ());

			strcat (pAct -> sKeyPresses, DLGetKeyString ());
			pAct -> iKeyPressesKeys = strlen (pAct -> sKeyPresses);
		}

		// DTXT3 ("Post DLPlayExt/DLGetDigitsExt, %d Keys = '%s' pressed\n", pAct -> iKeyPressesKeys, pAct -> sKeyPresses);
		
		if (pAct -> bNoWait)
			if (pAct -> iKeyPressesKeys == 0)	// no key (s) pressed during play
				bResponse = FALSE;

		if (bResponse)
			CmdPlayResponse (pAct);
	}
  
		// if any keys hit by caller, skip subsequint plays in this message
 
	if (pAct -> iKeyPressesKeys)
	{
		bCmdFinished = TRUE; 
		DTXT ("Key Pressed, MSG Command finshed\n");
	}
}


DLSrvHangUp ()

{

	pgm.bTerminateCall = TRUE;
//	DLHangUp ();
}


int ClientCmdExecute (XNPTR pXN)

{
int fillRet;
ACTION act;

	if (! pXN)
		return ERROR;

	InitActionStruct (& act, TRUE);
	fillRet = FillActionStructAndExecute (pXN, & act, 0);

	return SUCCESS;
}


int ReceivedPacketProcessing (char * dat)

{
XNPTR pXN;

	//DTXT2 ("((%s))\n", dat);

	XNDecomp (dat);

	pXN = XNGetRoot ();

	ClientCmdExecute (pXN);

	XNFreeMemory ();
}


SocketSendCallIdInfo ()

{
char buf [200];
char callid [100];
char line [100];
char datetime [100];

	if (callIdInfo.bCallingNumberValid)
		sprintf (callid, "%s%s%s", TAG_CALLID, callIdInfo.pCallingNumber, BackTag (TAG_CALLID));
	else
		* callid = 0;


	if (callIdInfo.bDialedNumberValid)
		sprintf (line, "%s%s%s", TAG_LINE, callIdInfo.pDialedNumber, BackTag (TAG_LINE));
	else
		* line = 0;

    	sprintf (buf, "<MSG><ACTION>ANSWER</ACTION>%s%s%s%s%s</MSG>",
		callid, line, TAG_DATETIME, SQLTimeDate (datetime), BackTag (TAG_DATETIME));

    	SocketSend (buf);
}


	//  loop used when this process has connected to a listening process
int ConnectSocketReadLoop ()
{
char * xmlRxCmd;
int ret;

	SocketSendCallIdInfo ();

    	for (;! pgm.bTerminateCall;) 
   	{ 
		//TXT ("ConnectSocketReadLoop () waiting for command from 'Client'\n");

        	ret = SocketRx (pgm.rxtxSocketHandle);  // and process the received msg
	
		//TXT2 ("ConnectSocketReadLoop (), %d ret from SocketRx ()\n", ret);

		if (ret == -1)	// loss of socket
			 break; 

		if (pgm.bTerminateCall) // || ! DLIsOffHook ())
		{
      			LogFileText ("Terminating Call"); // Lock File Text
			ClosePhoneLog ();

			// DTXT3 ("terminate = %d, IsOffHook () = %d\n", pgm.bTerminateCall, DLIsOffHook ());

			DTXT ("Terminating Call\n");

			SocketSend (CALL_TERMINATION_MSG);

			//DTXT2 ("%s sent to Newsticker, waiting 1 seconds\n", CALL_TERMINATION_MSG);
			sleep (1);
		}
	}
}


	// loop used when we wait for NT to connect to our listen ()
int ListenSocketReadLoop ()
{
int cliLen;

  //  printf("%s: waiting for data on port TCP %u\n","pgm",CLIENT_PORT);
    cliLen = sizeof(pgm.cliAddr);
    pgm.rxtxSocketHandle = accept(pgm.listenSocketHandle, (struct sockaddr *) & pgm.cliAddr, &cliLen);

    if (pgm.rxtxSocketHandle < 0)
    {
      perror("cannot accept connection ");
      return ERROR;
    }

    //printf ("port opened\n");
  
    while (1) 
       if (SocketRx (pgm.rxtxSocketHandle))
		break; 
}


int SocketCleanup ()

{

	// free buffers
   if (m_rxBufChars && m_pRxBuf)
   {
      free (m_pRxBuf);
      m_pRxBuf = 0;
      m_rxBufLen = 0;
   }

   if (m_txBufChars && m_pTxBuf)
   {
      free (m_pTxBuf);
      m_pTxBuf = 0;
      m_txBufLen = 0;
   }

	// close socket(s)
   if (pgm.rxtxSocketHandle)
     close (pgm.rxtxSocketHandle);

   if (pgm.listenSocketHandle)
      close (pgm.listenSocketHandle);
}




extern int chdev;

int CollectCallerIdInfo ()

{
int ret;
DX_EBLK * pDxEBLK;

char buffer [100];

	memset ((char *) & callIdInfo, 0, sizeof (callIdInfo));

	/* get callers name (use equates specific to your caller ID implementation) */

	if (DLGetCallerIDCallingNumber (callIdInfo.pCallingNumber))
		callIdInfo.bCallingNumberValid = TRUE;

	if (DLGetCallerIDDialedNumber (callIdInfo.pDialedNumber))
		callIdInfo.bDialedNumberValid = TRUE;

	return SUCCESS;
}


MainLoop ()

{
int ret;

   for (;;)     // for each call...
   {
      pgm.bTerminateCall = FALSE;
      pgm.bTerminatePgm = FALSE;

      DTXT1 ("Waiting for Call\n");
      LogFileText ("Waiting for Call"); // Lock File Text

      for (;;)
      {
	 if (pgm.bDisableCallerID)
	 {
            ret = DLWaitRing (-1);
	 }
	 else
	 {
            ret = DLWaitRingCallerID (-1);

	    if (ret == ERROR)
	    {
	       sleep (1);
	    }
	 }

	 if (ret == SUCCESS)
            break;
      }

             // call received, connect to NT
      DTXT1 ("Phone Ringing\n");
      DTXT1 ("Getting Caller ID\n");

      CollectCallerIdInfo ();
      DTXT1 ("Get Caller ID Returns\n");

      if (ConnectSocketInit ())
      {
         ConnectSocketReadLoop (pgm.rxtxSocketHandle);

	 DTXT ("TCP Connection to Client closed, Hanging Up\n");

         DLHangUp ();
         pgm.phoneOffhookI = FALSE;
      }
      else
      {
         DTXT1 ("   Failure to connect to Client!!!\n");
	 //return (ERROR);
      }

      DisconnectSocket ();
   }
}


void CtrlCTrap (int trash)

{
static int count = 0;

	DTXT ("CtrlCTrap ()\n");

	if (count++ > 3)
		exit (1);

	pgm.bTerminateCall = pgm.bTerminatePgm = TRUE;
}


int Init (int argc, char ** argv)

{
int idx, ret, help = FALSE;

	memset (& pgm, 0, sizeof (pgm));

	pgm.pgm = argv [0];

	for (idx = 1; idx < argc; idx ++)
	{
		if (! strcmp (argv [idx], "-d"))
			pgm.debugI = TRUE;

		else if (! strcmp (argv [idx], "-?"))
			help = TRUE;

		else if (! strcmp (argv [idx], "-i"))
		{
			if (idx +1 < argc)
			{
				strcpy (pgm.sIP, argv [idx +1]);
				pgm.bIP = TRUE;
				idx ++;	
				continue;
			}
			else
			{
				puts ("Error: Missing IP (either ddd.ddd.ddd.ddd or localhost");
				return ERROR;
			}
		}

		else if (! strcmp (argv [idx], "-p"))
		{
			if (idx +1 < argc)
			{
				pgm.iPort = atoi (argv [idx + 1]);
				pgm.bPort = TRUE;
				idx ++;	
				continue;
			}
			else
			{
				puts ("Error: Missing IP Port");
				return ERROR;
			}
		}

		else if (! strcmp (argv [idx], "-t"))
		{
			if (idx +1 < argc)
			{
				strcpy (pgm.sDialogicLine, argv [idx +1]);
				pgm.bDialogicLine = TRUE;
				idx ++;	
				continue;
			}
			else
			{
				puts ("Error: Missing Dialogic line (fmt = dxxxB1C1)");
				return ERROR;
			}
		}

		else if (! strcmp (argv [idx], "-h"))
   			pgm.bHangupAndExit = TRUE;

		else if (! strcmp (argv [idx], "-v"))
   			pgm.bVersionAndExit = TRUE;

		else if (! strcmp (argv [idx], "-c"))
			pgm.bDisableCallerID = TRUE;
		else 				// invalid option
		{
			TXT2 ("Invalid dlsrv command line option: '%s'\n\n", argv [idx]);
			help = TRUE;
		}
	}

	if (pgm.bVersionAndExit)
		return FALSE;

	if (help)
	{
		puts ("dlsrv:    help screen");
		puts ("");
		puts ("\t-i dd.dd.dd.dd \tIP of client (required)");
		puts ("\t-p dddd\t\tPort of client (required)");
		puts ("\t-t dxxxBdCd\tDialogic phone line to monitor for calls (required)");
		puts ("");
		puts ("\t-c\t\tNormal operation waits until the second ring to obtain Caller ID info");
		puts ("\t\t\tUse -c to DISABLE waiting for Caller ID (answer on the first ring)");
		puts ("\t-h\t\tHang up (open phone port, hang up, close, exit pgm)");
		puts ("\t-d\t\tDebugging turned on");
		puts ("\t-?\t\tThis help screen");
		puts ("");

		return (FALSE);
	}
   
	if (! pgm.bDialogicLine)
	{
		TXT ("Error, dialogic line unspecified.\n");
		return ERROR;
	}

 
	XNPrep (FALSE); ///MSGFALSE); //pgm.debugI);  // prep the XML decomposer

	ret = DLInit (pgm.sDialogicLine, pgm.debugI);
    
	if (ret == FILEBUSY)
	{
       		TXT2 ("Dialogic port %s is being used!\n", pgm.sDialogicLine);
		return ERROR;
    	}

    	if (ret != SUCCESS)
    	{
       		TXT1 ("Initialization of Dialogic board failed!\n");
		return ERROR;
    	}

    	DLHangUp ();
	pgm.phoneOffhookI = FALSE;

	if (pgm.bHangupAndExit)
		return ERROR;


	if (! (pgm.bIP && pgm.bPort))
	{
		TXT ("Error, IP and Port of client must be specified\n");
		return ERROR;
	}

			// interrupts
    	//DLInstallQuit ();     hanging, trap not cleaning up properly, so unused
        sigset (SIGINT, CtrlCTrap);
	sigset (SIGHUP, SIG_IGN);

	DTXT1 ("Initialization Successful\n");

	DTXT4 ("dlsrv running, DL Line: %s, Client: %s %d\n", pgm.sDialogicLine, pgm.sIP, pgm.iPort);

	if (pgm.bDisableCallerID)
	{
	   DTXT1 ("Initialization Successful, Answering on first ring (No Caller ID info)\n");
	}
	else
	{
	   DTXT1 ("Initialization Successful, Answering on the second ring (for Caller ID)\n");
	}

	return SUCCESS;
}


main (int argc, char ** argv)
 
{
char buf [100], * pt;
int ret;

    TXT2 ("\nDlSrv Ver %s\n\n", VER);

    if (! Init (argc, argv))
       exit (1);


    MainLoop ();

    SocketCleanup ();

    DLCleanup ();

    TXT2 ("%s: Exiting normally\n", pgm.pgm);
}
 

int DeliverRxPacket (char * m_pRxBuf, int m_rxBufChars)
{
char * buf;
char * pt;
int len = 0;

	buf = malloc (m_rxBufChars + 1);

	memcpy (buf, m_pRxBuf, m_rxBufChars);
	buf [m_rxBufChars] = 0;

	
	pt = strchr (buf, '\n');
	if (pt)
	{
		len = pt - buf + 1;
		* pt = 0;

		if (strlen (buf))
		{	
TXT2 ("\nRECEIVED: '%s'\n\n", buf);
       		    	ReceivedPacketProcessing (buf);
		}
	}

	free (buf);
	return len;
}



