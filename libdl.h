
#ifndef LIBDL_H
#define LIBDL_H

/* header file for the libdl.c  play record dialogic library */

#include <srllib.h>
#include <dxxxlib.h>

#define TRUE  1
#define FALSE 0
#define SUCCESS 1
#define ERROR 0

#define PLEN 256


#define OpenFile(a,b,c)  fopen (a, b)

#define POUND_KEY  '#'
#define STAR_KEY   '*'

#define DL_INIT_SUCCESS  1
#define DL_INIT_FAIL     0
#define DL_INIT_BUSY     2

#define PLAY_TERM_ANY_KEY   1
#define PLAY_TERM_POUND_KEY 2
#define PLAY_TERM_STAR_KEY  3
#define PLAY_TERM_NO_KEY    4

     /* get digit last use flag */

#define USR_DIG_NO_ENTERED   0
#define USR_DIG_ENTERED      1

#define DL_LOCK_STAT_UNLOCKED     1
#define DL_LOCK_STAT_LOCKED       2
#define DL_LOCK_ERROR             3



/* Lock File codes (first digit in the file contents) */

enum {DL_PHONE_UNUSED = 0,
      DL_PHONE_WAITING_FOR_CALL,
      DL_PHONE_CALL_IN_PROGRESS};


int DLInit (char * port, int debug);
void DLQuit (int trash);
void DLCleanup ();
void DLInstallQuit ();

int DLSetPostPlayHook (int (* postPlayFn) ());
int DLRecord (char * fname, int maxSeconds, int enterKey);
int DLPlay (char * fname);
int DLPlayExt (char * fname, int maxSeconds, int maxDigits, int enterKey, int ignoreInput);
int DLGetDigitsExt (int maxSeconds, int maxDigits, int enterKey);
char * DLGetKeyString ();
char * WaitCallErrString ();
char * DLLockFileName (char * buf, int portno);
char * DLLockFileNamePath (char * buf, int portno);
int DLLockFileText (char * txt, int statI);
int DLWaitCall ();
int DLCallerId (int bEnable);
int DLWaitRing (int seconds);
DX_EBLK * DLReturnWaitRingDX_EBLK ();
int DLSetPlayTerm (int stat);
int DLReturnTermCode ();
int DLHangUp (); 
int DLIsOffHook ();
int DLCheckTerm ();
void DLClearKeyBuffer ();

int DLGetCallerIDCallingNumber (char * buffer);
int DLGetCallerIDDialedNumber (char * buffer);
int DLLockeFileTextNOLock (int board, int port, char * buf);

#endif

