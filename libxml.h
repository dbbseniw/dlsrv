//
// libxml.h : header file for the XML decomposition funtions
//

#ifndef _LIBXML_H
#define _LIBXML_H

//#include "stdafx.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "malloc.h"




#define TRUE 1
#define FALSE 0

struct XML_NODE;

typedef struct XML_NODE
{
	char * pXmlTag;
	char * pData;
	struct XML_NODE * pChild;
	struct XML_NODE * pNext;
}XN, * XNPTR;

XNPTR pXNRoot;


void XNPrep (int debug);
void XNFreeMemory ();
void XNDecomp (char * rdat);
XNPTR XNGetRoot ();





#endif

