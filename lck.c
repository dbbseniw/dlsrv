#define ALLOCATE
#include <stdio.h>
#include <locks.h>
#include <fileio.h>
#include <signal.h>
#include <sys/fcntl.h>

FILE * lock_fp;

#define PLEN 100
#define TRUE 1
#define FALSE 0


char port_C [] = "dxxxB1C1";

#define NOTIFY              1
#define NONOTIFY            0


SetLock(fname,fp,type,retries,notify)
char *fname;
FILE *fp;
int type;
int retries;
int notify;
{
   int fd, ret, cnt, msgup;
   struct flock lck;
   static char func[] = "SetLock";

   msgup = cnt = 0;                         /* zero this out */

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   do
   {
      ret = fcntl(fd,F_SETLK,&lck);         /* try to lock file */

      if(ret < 0)                           /* could not lock */
      {
         if(msgup == 0)                     /* message not displayed */
         {
            printf ("bush\n");
            msgup = 1;                      /* do it, set flag */
         }

         sleep(1);                          /* so wait */
         cnt++;
      }
   }
   while((ret < 0) && (cnt < retries));

   if(ret < 0)                              /* lock failed */
   {
      if(notify == NOTIFY)                  /* notify user ? */
         printf ("Err\n");

      ret = FILEBUSY;                       /* set error */
   }

   return(ret);
}





char * DLLockFN ()

{
static char buf [PLEN + 1];

   sprintf (buf, "/usr/spool/uucp/LCK..%s", port_C);
   
   return (buf);
}

int DLLockPort ()

{
int ret;

                                      /* open existing file */
   lock_fp = fopen (DLLockFN (), UPDT);
   
   fflush (lock_fp);                  /* force it to disk to be locked */
   
   if (lock_fp)
   {
      ret = SetLock (DLLockFN (), lock_fp, F_WRLCK, 1, 0);

      if (ret == 0)                   /* Sucessfull */
      {
         fprintf (lock_fp, "%d", getpid ());
         fflush (lock_fp);
         
         return (SUCCESS);
      }
      
      fclose (lock_fp);
      lock_fp = 0;
      
      return (FILEBUSY);
   }
                                       /* lock file DNE */
   lock_fp = fopen (DLLockFN (), WR); /* for 666o */
   
   fflush (lock_fp);                  /* force it to disk to be locked */
   
   if (lock_fp)
   {
      ret = SetLock (DLLockFN (), lock_fp, F_WRLCK, 1, 0);

      if (ret == 0)                   /* Sucessfull */
      {
         fprintf (lock_fp, "%d", getpid ());
         fflush (lock_fp);
         
         return (SUCCESS);
      }
      
      fclose (lock_fp);
      lock_fp = 0;
      
      return (FILEBUSY);
   }
   
   return (ERROR);
}

int DLUnLockPort ()

{

   if (lock_fp)
   {
      SetLock ("", lock_fp, F_UNLCK, 1, 0);

      unlink (DLLockFN ());
   }
   
   lock_fp = 0;
}


CkLock(fname,fp,type,retries,notify)
char *fname;
FILE *fp;
int type;
int retries;
int notify;
{
   int fd, ret, cnt, msgup;
   struct flock lck;
   static char func[] = "CkLock";

   msgup = cnt = 0;                         /* zero this out */

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   ret = fcntl(fd,F_GETLK,&lck);         /* try to lock file */

   if(ret < 0)                           /* could not lock */
   {
      return (FALSE);
   }

   return (lck.l_type != F_UNLCK);
}


int DLPortLocked ()

{
int ret;
FILE * lock_fp;  /* local FILE * */

                                      /* open existing file */
   lock_fp = fopen (DLLockFN (), UPDT);

   if (lock_fp)
   {
      ret = CkLock (DLLockFN (), lock_fp, F_WRLCK, 1, 0);

      if (ret == 1)                   /* Successfull */
      {
         return (TRUE);
      }
   }
   /* else                            file dne, so not locked */
      
   return (FALSE);
}


main (int argc)

{

   if (argc == 2)
   {
   printf ("DLLockPort () returned %d\n", DLLockPort ());
   
   printf ("sleeping for 60\n");
   fflush (stdout);
   
   sleep (60);
   
   DLUnLockPort ();
   }
   else
   {
      printf ("port %s is %s\n", port_C, DLPortLocked () ? "Locked" : "Unlocked");
   }
}
