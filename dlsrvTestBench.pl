#!/usr/local/bin/perl
#
#	waits for dlsrv to connect, then waits for tester to connect, then
#	pipes comm between
#

use strict;
use IO::Socket;
use IO::Select;

	$|++;

my $acceptSock = new IO::Socket::INET (
		 LocalHost => '192.168.1.102',
                 LocalPort => 3040,
                 Proto => 'tcp',
                 Listen => 1,
                 Reuse => 1,
                 );

	print "dlsrvTestBench listening on port 3040 for telnet\n";

my     	$tnSock = $acceptSock -> accept ();
my $oldfh = select($tnSock); $|++; select($oldfh);

        print "Accepted on 3040\n";


my $acceptSock = new IO::Socket::INET (
		 LocalHost => '192.168.1.102',
                 LocalPort => 3030,
                 Proto => 'tcp',
                 Listen => 1,
                 Reuse => 1,
                 );

	print "dlsrvTestBench listening on port 3030 for dlsrv\n";

my     	$dlsrvSock = $acceptSock -> accept ();
my $oldfh = select($dlsrvSock); $|++; select($oldfh);

        print "Accepted on 3030\n";



		# using select
	while (1)
	{
	my $select = new IO::Select ($dlsrvSock);
		if ($select -> can_read (0))
		{
		my $dat;
			sysread ($dlsrvSock, $dat, 100);
			print $tnSock $dat;
		}
		else
		{
			print '-'
		}

	my $select = new IO::Select ($tnSock);
		if ($select -> can_read (0))
		{
		my $dat;
			sysread ($tnSock, $dat, 100);
			print $dlsrvSock $dat;
		}
		else
		{
			print '|'
		}

		select (undef, undef, undef, .1);
	}

