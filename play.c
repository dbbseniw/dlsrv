
#define ALLOCATE

#include <stdio.h>
#include "libdl.h"
#include "locks.h"

extern char DLtest_key;


main (int argc, char ** argv)
 
{
char * pt;
char fname [100], * arg;
int done = 0;

   if (argc == 1)
   {
      printf ("usage: play msg_file_name\n");
      return (0);
   }
 
   if (! DLInit ("dxxxB1C1", 10))
   {
      fprintf (stderr, "Error initializing the DiaLogic board\n");
      exit (0);
   }
   else
      fprintf (stderr, "Port dxxxB1C1 opened\n");
 
   DLInstallQuit ();
   
   arg = argv [1];
   
   printf ("\n\nPhone the DCM system to hear this recording. (945-1173)B\n");
   printf (" (press ^C to cancel)\n");

   for (;;)
      if (DLWaitCall (60))
         break;

   if (strlen (arg) > 4) /* might have a .vox */
   {
      if (! strcmp (".vox", arg + strlen (arg) - 4)) /* yup */
      {
         printf ("\nEnter (phone) a multi digit code and # to terminate message early\n");
      
         DLSetPlayTerm (PLAY_TERM_POUND_KEY);
         
         DLPlay (arg);
         
         done = 1;
      }
   }
   
   if (! done)
   {
/*       printf ("\nPress any (phone) key terminate message early\n\n");*/
       
       sprintf (fname, "%s.vox", arg);
          
       printf ("\nPlaying msg (1)\n");
       DLPlay (fname);
         
         printf ("\nTransmitting forward\n");
         DLForward ("&*73516");
         
         printf ("\nPlaying msg (2)\n");
         DLPlay (fname);
   }
 
   pt = DLGetKeyString ();
        
   if (pt)
      if (strlen (pt))
         printf ("key string = %s\n", pt);
      else
         printf ("No key string entered\n");
             
   DLCleanup (1);
}
 
 
