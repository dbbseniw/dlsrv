
//   dialogic test pgm


#include <stdio.h>
#include <srllib.h>
#include <dxxxlib.h>
#include <signal.h>

#define TRUE 1
#define SUCCESS 1
#define FALSE 0
#define ERROR 0


int chdev;
int debug_I = 1;


int DLIsOffHook ()

{
int offHook = 0;
long lineState;
                                                 /* check for off hook */
                                                 /* or loop current */
                                                 /* either is off hook condition */

// originally my understanding was that if EITHER the hook state, or the loop current indicators
// showed off hook, then the line was off hook.

// But testing on a SO Bell line showed, both indicated off hook correctly, but when
// the caller hung up, the RLS_HOOK was still showing off hook (incorrectly)
//
// So for the moment, if BOTH show off hook, then OFF hook, else, ON hook.
//


   lineState = ATDX_LINEST (chdev);              /* get current line state */

   if (! (lineState & RLS_HOOK))
   {
      offHook ++;

       if (debug_I)
       {
         fprintf (stderr, "   lineState & RLS_HOOK  ->  OffHook\n");
         fflush (stderr);
      }
   }

   if (! (lineState & RLS_LCSENSE))
   {
      offHook ++;

      if (debug_I)
      {
         fprintf (stderr, "   lineState & RLS_LCSENSE  ->  OffHook\n");
         fflush (stderr);
      }
   }

   return offHook == 2;
}




#define HOOKCK  printf ("Currently - %s\n", DLIsOffHook () ? "OFF HOOK" : "HUNG UP");fflush (stdout);

#define STATECK printf ("State Currently - %s\n", ATDX_STATE (chdev) == CS_IDLE ? "IDLE" : "NOT IDLE");fflush (stdout);


void STATECK1 (char * a)

{
int state = ATDX_STATE (chdev);

    if (state == CS_IDLE)
       printf ("%s:State Currently - IDLE\n", a);
    else
       printf ("%s:State Currently - state = %x\n", a, state);

   fflush (stdout);
}

void DLQuit (int trash)

{
int setret, ret;
static int count = 0;



fprintf (stderr, "DLQuit\n");
fflush (stderr);


//
//
//  The next line will cause corruption in the DL memory/driver during a ^C interrupt call
//

look here.

STATECK1("q");

#ifdef TRADSHSHS
dx_sethook (chdev, DX_ONHOOK, EV_SYNC);

exit (1);
   if (count ++)
{

   printf ("DupDLQuit\n");
      fflush (stdout);

   return ;
}

   fprintf (stderr, "DLQuit (), pre HOOKCK\n");
   fflush (stderr);

HOOKCK
   if (debug_I)
   {
      fprintf (stderr, "DLQuit (), pre DLHangUp()\n");
      fflush (stderr);
   }
  // setret = DLHangUp ();
     ret = dx_sethook (chdev, DX_ONHOOK, EV_SYNC);

   if (debug_I)
   {
      fprintf (stderr, "DLQuit (), pre dx_close (chdev)\n");
      fflush (stderr);
   }
HOOKCK

#endif

}



main ()

{
int ret;
DX_EBLK waitRing_DXEBLK;



printf ("Opening B1C1\n");

chdev = dx_open ("dxxxB1C1", 0);    // ex: "dxxxB1C1", 0);   board 1 channel 1 *

printf ("B1C1 = chdev = %d\n\n", chdev);

   //signal (SIGINT, DLQuit);
   sigset (SIGINT, DLQuit);

     ret = dx_sethook (chdev, DX_ONHOOK, EV_SYNC);

    memset ((char *) & waitRing_DXEBLK, 0, sizeof (waitRing_DXEBLK));

    ret = dx_setevtmsk (chdev, DM_RINGS);

    printf ("dx_setevtmsk () ret %d\n", ret);

       /*
       * Set to answer after MAXRING rings
       */

      if ( dx_setrings( chdev, 1) == -1 ) 
      {
         fprintf( stderr, "dx_setrings() failed for %s",
                ATDV_NAMEP( chdev ) );

         return (ERROR);
      }

HOOKCK
STATECK1("Waiting");


//    printf ("waiting for a call\n");

 //   ret = dx_getevt (chdev, & waitRing_DXEBLK, 10);

  //  printf ("Post dx_getevt () ret %d, %s\n", ret,
   //     ret == -1 ? "No Call" : "Call Received");

ret = 0;

    if (! ret)
    {
     ret = dx_sethook (chdev, DX_OFFHOOK, EV_SYNC);

HOOKCK
STATECK

    printf ("4 seconds\n");
sleep (4);

printf ("Post 4 sec Setting ON_HOOK\n");
     ret = dx_sethook (chdev, DX_ONHOOK, EV_SYNC);
HOOKCK
STATECK

    printf ("10 seconds\n");
sleep (10);

HOOKCK
STATECK
    }
}



