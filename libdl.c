/*************************************************************************
**
** name:           Dialogic interface library
**
** purpose:        
**
** return
** codes:          
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

#include <signal.h>
#include <stdio.h>
#include <sys/fcntl.h>
#include <unistd.h>

#include "libdl.h"
#include "locks.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

//#define STAT_TRACE


#ifdef STAT_TRACE2
extern Stat ();
#endif


#define MAX_RECORD_PLAY_TIME  (60 * 10 * 3)    // in tenths of a second     (currently 3 min)


#define MAXTPTTERMS 12

char port_C [100] = "";
char keylog_C [100] = "";
int chdev, debug_I = 0;
int        debug_I0 = 0;   // (always FALSE)
int wtring_rings_I = 1;

int last_call_ret_I = -2;

FILE * lock_fp = 0;

int play_term_I = PLAY_TERM_ANY_KEY;
int dig_stat_I = USR_DIG_NO_ENTERED;

int termCode = 0;		// reason for termination from the last play/record, or GetDigits ()

int (* postPlayFn) () = 0;





/* 

	0  = Locked
	-1 = Lock Failed
*/


DLSetLock (FILE * lockFp)

{
int fd = fileno (lockFp);

       	if (flock (fd, LOCK_EX | LOCK_NB))
		return (-1);

	return (0);
}


DLResetLock (FILE * lockFp)

{
int fd = fileno (lockFp);

        flock (fd, LOCK_UN);

	return (0);
}





CvtChr (char * text, int len, char from, char to)

{

   for (; len > 0; len --, text ++)
      if (* text == from)
         * text = to;
}



int DLSetMinAnswerRings (int rings)

{

   wtring_rings_I = rings;
   
   return (rings);
}


/***************************************************************************
 *        NAME: int DLCheckTerm ()
 * DESCRIPTION: This routine checks the termination after a dx_rec or 
 *              dx_play or dx_getdig looking for fatal termination types.
 *              If any are found, we longjmp back to an idle state, else 
 *              we return to the caller.
 *      INPUTS: none.
 *     OUTPUTS: none
 *     RETURNS: none
 *       CALLS: none
 *    CAUTIONS: This routine does not return to caller if a fatal 
 *              termination is found.
 *************************************************************************/

int  DLCheckTerm ()
{

   if (ATDX_TERMMSK (chdev) & TM_MAXNOSIL)
   {
      if (debug_I)
      {
         printf ("Call Termination MAXNOSIL\n");
         fflush (stdout);
      }
         
      return (TRUE);
   }
   
   if (ATDX_TERMMSK (chdev) & TM_LCOFF)
   {
      if (debug_I)
      {
         printf ("Call Termination LCOFF\n");
         fflush (stdout);
      }
         
      return (TRUE);
   }
   
   if (ATDX_TERMMSK (chdev) & TM_PATTERN)
   {
      if (debug_I)
      {
         printf ("Call Termination PATTERN\n");
         fflush (stdout);
      }
         
      return (TRUE);
   }
   
   if (ATDX_TERMMSK (chdev) & TM_USRSTOP)
   {
      if (debug_I)
      {
         printf ("Call Termination USRSTOP\n");
         fflush (stdout);
      }
         
      return (TRUE);
   }

   return FALSE;
}


/***************************************************************************
 *              DLLoopDrop.c
 *        NAME: int DLLoopDrop ()
 *       CALLS: none
 *************************************************************************/

int  DLLoopDrop ()
{

   if (ATDX_TERMMSK (chdev) & TM_LCOFF)
   {
      if (debug_I)
      {
         printf ("Call Termination LCOFF\n");
         fflush (stdout);
      }
         
      return (TRUE);
   }
   
   return FALSE;
}


/***************************************************************************
 *        NAME: static USHORT bld_tdig_msk
 * DESCRIPTION: Return the terminator mask for the ASCII DTMF
 *      INPUTS: termdigsp - terminating digits ASCIIZ string.
 *     OUTPUTS: none.
 *     RETURNS: bit mask built for all valid terminating digits.
 *       CALLS: none.
 *    CAUTIONS: none.
 *************************************************************************/

static unsigned short 
bld_tdig_msk(termdigsp)
char    *termdigsp;
{
    static char  dtmfs[16]= {'d','1','2','3','4','5','6','7','8','9','0',
                                                    '*','#','a','b','c'};
    register unsigned short i;
    unsigned short          mask;

    /* loop on all terminating digits, and create the mask */   

    mask = 0;
    for ( ; *termdigsp; termdigsp++) {
        for(i = 0; i < 16; i++) {
            if (*termdigsp == dtmfs[i]) {
                mask |= ((unsigned short)1 << i);
                break;
            }
        }
    }
    return (mask);
}


/*************************************************************************
**
** name:           DLLockFN.c
**
** purpose:        Generate the lock file name for the dl port
**
** return
** codes:          The lock file name
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 7/97 - ds
**
**************************************************************************/

char * DLLockFN ()

{
static char buf [PLEN + 1];

   sprintf (buf, "/var/lock/LCK..%s", port_C);
   
   return (buf);
}


/* DLLockFileName.c */
char * DLLockFileName (char * buf, int portno)

{
int board, port;

   board = (portno - 1) / 4 + 1;
   port = (portno - 1) % 4 + 1;   

   sprintf (buf, "B%dC%d", board, port);
   
   return (buf);
}


/* DLLockFileNamePath.c */
char * DLLockFileNamePath (char * buf, int portno)

{
int board, port;

   board =(portno - 1) / 4 + 1;
   port = (portno - 1) % 4 + 1;   

   sprintf (buf, "/usr/spool/uucp/LCK..dxxxB%dC%d", board, port);
   
   return (buf);
}


/* DLLockStatus.c */
int DLLockStatus (int port, char * text)

{
char file [PLEN + 1];
int ret, locked;
FILE * fp;

   DLLockFileNamePath (file, port);

   sprintf (text, "%dUnused", DL_PHONE_UNUSED);
   
   if (access (file, F_OK))                /* DNE */
   {
      return (DL_LOCK_STAT_UNLOCKED);
   }

   fp = fopen (file, "r");
   if (! fp)
   {
      strcpy (text, "Lock File UnOpenable");
      return (DL_LOCK_ERROR);
   }

   locked = CkLock (file, fp, F_WRLCK, 1, FALSE);

   if (locked)
   {
   char * pt;
   int len = 0;
   
      * text = 0;
      
      pt = fgets (text, 79, fp);
      
      if (pt)
      {
         CvtChr (text, strlen (text), '\r', 0);
         CvtChr (text, strlen (text), '\n', 0);
         
         len = strlen (pt);
      }
      
      if (len)
         text [len < 80 ? len : 79] = 0;
      
      if (! * text)
         strcpy (text, "Waiting for a call");
      
      ret = DL_LOCK_STAT_LOCKED;
   }
   else
   {
      sprintf (text, "%dUnused", DL_PHONE_UNUSED);
      ret = DL_LOCK_STAT_UNLOCKED;
   }
   
   fclose (fp);
   return (ret);
}


/* DLLockFileText.c */
DLLockFileText (char * txt, int statI)

{
char buf [100];

   if (statI > -1)
      sprintf (buf, "%d%s", statI, txt);
   else
      sprintf (buf, "%s", txt);
   
   if (0 && debug_I)
   {
      printf ("DLLockFileText (), setting contents to '%s'\n", buf);
      fflush (stdout);
   }
   
   if (lock_fp)
   {
      fseek (lock_fp, 0, 0);
      fflush (lock_fp);

      fwrite (buf, strlen (buf) + 1, 1, lock_fp);
      fflush (lock_fp);
   }
   else
   {
      printf ("DLLockFileText (), ERROR:  lock_fp == 0\n");
      fflush (stdout);
   }
}



int DLLockFileTextNOLock (int board, int line, char * buf)

{
char lfn [256];
FILE * fp;
int ret = 0;
	
   sprintf (lfn, "/var/lock/LCK..dxxxB%dC%d", board, line);

   fp = OpenFile (lfn, "r", FALSE);
   
   if (fp)
   {
      ret = fread (buf, 1, 100, fp);
      fclose (fp);
   }

   return (ret);
}


/*************************************************************************
**
** name:           DLLockPort.c
**
** purpose:        Attempt to lock the indicated port.
**
** return
** codes:          SUCCESS, ERROR, or FILEBUSY
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLLockPort ()

{
int ret;

   if (debug_I)
   {
      //printf ("DLLockPort () locking file %s\n", DLLockFN ());
      //fflush (stdout);
   }
                                      /* open existing file */
   lock_fp = OpenFile (DLLockFN (), "r+", FALSE);
   
   if (lock_fp)
   {
      if (debug_I)
      {
         //printf ("DLLockPort (), fflushing %s  (fp = %d)\n", DLLockFN (), lock_fp);
         //fflush (stdout);
      }
    
      fflush (lock_fp);                  /* force it to disk to be locked */
   
      if (debug_I)
      {
         //printf ("DLLockPort (), Locking (%s)\n", DLLockFN ());
         //fflush (stdout);
      }
    
      // (original DCM) ret = SetLockQuiet (DLLockFN (), lock_fp, F_WRLCK, 1, 0);

      ret = DLSetLock (lock_fp);

      if (ret == 0)                   /* Successfull */
      {
      char buf [50];
      
         sprintf (buf, "Process # %d", getpid ());
         if (debug_I)
         {
            //printf ("DLLockPort (), (%s) contents set to '%s'\n", DLLockFN (), buf);
            //fflush (stdout);
         }
         
         DLLockFileText (buf, 0); //DL_PHONE_UNUSED);

         return (SUCCESS);
      }
      else
      {
         if (debug_I)
         {
            printf ("DLLockPort (\"UPDT\"), Locking Failed\n");
            fflush (stdout);
         }
      
         fclose (lock_fp);
         lock_fp = 0;
      }
      
      return (ERROR); //FILEBUSY);
   }
                                       /* lock file DNE */
   lock_fp = OpenFile (DLLockFN (), "w", TRUE); /* for 666o */
   
   if (lock_fp)
   {
      fflush (lock_fp);               /* force it to disk to be locked */
      
      // Original DCM  ret = SetLock (DLLockFN (), lock_fp, F_WRLCK, 1, 0);
      ret = DLSetLock (lock_fp);

      if (ret == 0)                   /* Sucessfull */
      {
      char fbuf [20];
      
         sprintf (fbuf, "DL Init %d", getpid ());
         DLLockFileText (fbuf, DL_PHONE_UNUSED);
         
         chmod (DLLockFN (), 438);   /* 666o */
         
         return (SUCCESS);
      }
      
      if (debug_I)
      {
         printf ("DLLockPort (\"WR\"), Locking Failed\n");
         fflush (stdout);
      }
      
      fclose (lock_fp);
      lock_fp = 0;
      
      return (ERROR); //FILEBUSY);
   }
   
   return (ERROR);
}


/*************************************************************************
**
** name:           DLPortLocked.c
**
** purpose:        Check to see if port is locked.
**
** return
** codes:          TRUE or FALSE
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLPortLocked ()

{
int ret;
FILE * lock_fp;  /* local FILE * */

                                      /* open existing file */
   lock_fp = fopen (DLLockFN (), "r+");

   if (lock_fp)
   {
      ret = CkLock (DLLockFN (), lock_fp, F_WRLCK, 1, 0);

      if (ret == TRUE)                   /* Successfull */
      {
         return (TRUE);
      }
   }
   /* else                            file dne, so not locked */
      
   return (FALSE);
}


/*************************************************************************
**
** name:           DLUnlockPort.c
**
** purpose:        Unlock the previously locked port
**
** returnna
** codes:          
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLUnLockPort ()

{

   if (lock_fp)
   {
      // origina DCM SetLock ("", lock_fp, F_UNLCK, 1, 0);

      DLResetLock (lock_fp);

      fclose (lock_fp);
      
      unlink (DLLockFN ());
   }
   
   lock_fp = 0;
}


int DLReturnTermCode ()

{

	return termCode;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////

DebugATDX_TERMSK (char * dlFunc) 

{

   termCode = ATDX_TERMMSK (chdev);

   if (debug_I)
   {
	switch (termCode)
	{
		case TM_EOD:
			printf ("	\t%s () reached EndOfData without termination criteria\n", dlFunc);
			break;

		case TM_MAXDTMF:
			printf ("	\t%s () returned, max digits reached\n", dlFunc);
			break;

		case TM_DIGIT:
			printf ("	\t%s () returned, specified digit received\n", dlFunc);
			break;

		case TM_LCOFF:
			printf ("	\t%s () returned, Call Terminated by caller\n", dlFunc);
			break;

		case TM_IDDTIME:
			printf ("	\t%s () returned, specified InterDigit Delay reached\n", dlFunc);
			break;

		case TM_ERROR:
			printf ("	\t%s () returned, IO Device Error\n", dlFunc);
			break;

		case TM_BARGEIN:
			printf ("	\t%s () returned, Play terminated due to Barge-in\n", dlFunc);
			break;


		default:
      			printf ("       \t%s () returned, TERMMSK = %x\n", dlFunc, termCode);
			break;
	}

      fflush (stdout);
   }
}

/*************************************************************************
**
** name:           DLRecord.c
**
** purpose:        Record a message using the supplied file name.
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:    Terminate at phone key press, or 5 seconds of silence
**                 or 20 seconds max.
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**          8/5    Modified to use the .vox8 suffix (force the use even
**                 when the argument name is only .vox.    All recording
**                 is now done at 8khz
**
**************************************************************************/

int DLRecord (char * fname, int maxSeconds, int enterKey)

{
int cnt, num, fd, ret;
long term;
DX_IOTT iott;
DV_DIGIT digp;

DV_TPT rec_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_DIGMASK,0,TF_DIGMASK,0,0},
	{IO_CONT,DX_MAXDTMF,1,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_EOT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0},   // max record time of 3 minutes
};	

char * pSuffix;
char fNameVox8 [1024];

   if (dx_clrdigbuf (chdev) == -1)  /* clear previously entered digits */
      return (0);

   rec_tpt [3].tp_length = maxSeconds * 10;    // 100 ms ticks

   if (enterKey)
      rec_tpt [0].tp_length = enterKey;   // DIGMASK



      // First extract the suffix including the '.', compare it to ".vox"
   pSuffix = strrchr (fname, '.');

   if (pSuffix)
   {
	    // if .vox suffix set a flag
      if (! strcmp (pSuffix, ".vox"))
      {
	 strcpy (fNameVox8, fname);
	 strcpy (fNameVox8 + strlen (fNameVox8), "8");

	 fname = fNameVox8;
      }
   }

   printf ("\tRecording %s at 8khz\n", fname);

   fd = open (fname, O_RDWR | O_TRUNC | O_CREAT, 0666 );

   if (fd == -1)
      return (0);

   /*
    * Clear and Set-Up the IOTT strcuture
    */
      memset (& iott, 0, sizeof (DX_IOTT));

      iott.io_type = IO_DEV | IO_EOT;
      iott.io_fhandle = fd;
      iott.io_offset = (long) 0;
      iott.io_length = -1;

   /*
    * Record VOX File on D/4x Channel
    */

   if (debug_I)
   {
      printf ("calling dx_rec\n");
      fflush (stdout);
   }
    
   ret = dx_rec (chdev, & iott, rec_tpt, (unsigned short)(RM_TONE | EV_SYNC | PM_SR8));
  
   DebugATDX_TERMSK ("DLRecordEx"); 
 
   close (fd);
   
   return (1);
}


void DLClearKeyBuffer ()

{

   dx_clrdigbuf (chdev);

   * keylog_C = 0;
   dig_stat_I = USR_DIG_NO_ENTERED;
}


/* DLSetPostPlayHook.c */
int DLSetPostPlayHook (int (* postPlay) ())
{
 
   postPlayFn = postPlay;

   return SUCCESS;
}


/*************************************************************************
**
** name:           DLPlay.c
**
** purpose:        Play the indicated audio segment.
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLPlay (char * fname)

{
char * pt;
int cnt, num, ret;
long term;
DX_IOTT iott;

DV_TPT * tptp;

             /* note DIGMASK is always first, to be modified */

       // type, term type, length, flags, data, reserved, next ptr

DV_TPT any_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_DIGMASK,0,TF_DIGMASK,0,0},
	{IO_CONT,DX_MAXDTMF,1,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_EOT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0}
};	
	
DV_TPT pound_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_DIGMASK,0,TF_DIGMASK,0,0},
	{IO_CONT,DX_MAXDTMF,30,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_EOT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0},
};	
	
DV_TPT no_key_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_MAXDTMF,50,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_EOT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0},
};	
	
DV_DIGIT digp;

   /* set termination parameters */

   if (play_term_I == PLAY_TERM_ANY_KEY)
   {
      if (debug_I)
      {
         printf ("DLPlay (), term criteria == PLAY_TERM_ANY_KEY\n");
         fflush (stdout);
      }
      
      tptp = any_tpt;
   }
   else 
      if (play_term_I == PLAY_TERM_POUND_KEY ||
          play_term_I == PLAY_TERM_STAR_KEY)    /* collect until # || * key */
      {
         if (debug_I)
         {
            printf ("DLPlay (), term criteria == PLAY_TERM_POUND_KEY\n");
            fflush (stdout);
         }
      
         tptp = pound_tpt;
      
         tptp -> tp_length = ((play_term_I & PLAY_TERM_POUND_KEY) ? DM_P : 0) |
                             ((play_term_I & PLAY_TERM_STAR_KEY)  ? DM_S : 0);
      
         /*tptp -> tp_length = bld_tdig_msk ("#");*/
      }
   else
      tptp = no_key_tpt;
    
   
   //if (dx_clrdigbuf (chdev) == -1)  /* clear previously entered digits */
     // return (0);

   memset ((void *) & iott, 0, sizeof (iott));
   iott.io_type = IO_DEV | IO_EOT;
   iott.io_bufp = 0;
   iott.io_offset = (unsigned long) 0;
   iott.io_length = -1;          /* play to eof */
   
   if ((iott.io_fhandle = open (fname, O_RDONLY)) == -1)
   {
      printf ("Error opening %s\n", fname);
      return (0);
   }

   if (debug_I)
   {
      printf ("%s opened, dx_play called\n", fname);
      fflush (stdout);
   }

   ret = dx_play (chdev, & iott, tptp, (unsigned short) EV_SYNC);

   if (debug_I)
   {
      //printf ("dx_play returned with %d\n", ret);
      fflush (stdout);
   }

   term = ATDX_TERMMSK (chdev);
   
   if (debug_I)
   {
      printf ("TERMMSK = %x\n", term);
      fflush (stdout);
   }

   if (term & TM_MAXDTMF || term & TM_DIGIT) /* key (s) expected) */
   {
      num = dx_getdig (chdev, tptp, & digp, EV_SYNC);
      
      num --;
      
      if (debug_I)
      {
         printf ("dx_getdig indicates that there is %d digits entered\n", num);
         fflush (stdout);
      }
      
      * keylog_C = 0;                            /* start with a null string */
      
      for (cnt = 0, pt = keylog_C; cnt < num; cnt ++, pt ++)
      {
         * pt = digp.dg_value [cnt];
         if (debug_I == 44)
         {
            printf ("Digit rx = %c, digit type = %d\n", * pt, digp.dg_type [cnt]);
            fflush (stdout);
         }
      }
      
      * pt = (char) 0;
      
      if (num)
         dig_stat_I = USR_DIG_ENTERED;
      else
         dig_stat_I = USR_DIG_NO_ENTERED;
      
   }
   else
   {
      if (debug_I)
      {
         printf ("play received 0 keys during play\n");
         fflush (stdout);
      }
      
      dig_stat_I = USR_DIG_NO_ENTERED;
   }
   
   close (iott.io_fhandle);

   if (postPlayFn)
      (* postPlayFn) ();
   
   return (ret);
}



/*************************************************************************
**
** name:           DLGetDigitsExt.c
**
** purpose:        Collect keypresses, details included in the call.
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:     enterKey = DM_0 - DM_9, DM_S (*) DM_P (#), or 0 (no enter key)
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLGetDigitsExt (int maxSeconds, int maxDigits, int enterKey)

{
char * pt;
int cnt, num, ret = 0;
int getDigRet;
long term;

DV_TPT * tptp;

             /* note DIGMASK is always first, to be modified */

       // type, term type, length, flags, data, reserved, next ptr

DV_TPT universal_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_DIGMASK,0,TF_DIGMASK,0,0},
	{IO_CONT,DX_MAXDTMF,100,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_CONT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0},
	{IO_EOT,DX_IDDTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0}
};	
	
DV_DIGIT digp;

	if (debug_I)
		printf ("DLGetDigitExt (%d seconds, %d digits, EnterKey = %d)\n", 
			maxSeconds, maxDigits, enterKey);

   /* set termination parameters */

   universal_tpt [4].tp_length = maxSeconds * 10;    // 100 ms ticks
   universal_tpt [1].tp_length = maxDigits;   

   if (enterKey)
      universal_tpt [0].tp_length = enterKey;
   
   tptp = universal_tpt;

   num = getDigRet = dx_getdig (chdev, tptp, & digp, EV_SYNC);
     
   if (getDigRet == -1)
   {
      printf ("DLGetDigitsExt():dx_getdig() returned ERROR, (%d, %s)\n", ATDV_LASTERR (chdev), ATDV_ERRMSGP (chdev));
      fflush (stdout);
   }
 
   term = ATDX_TERMMSK (chdev);

   if (term == TM_LCOFF)
	ret = -1; 
                        
      DebugATDX_TERMSK ("DLGetDigitsEx:dx_getdig"); 

      num --;
      
      if (debug_I)
      {
         printf ("dx_getdig indicates that there is %d digits entered\n", num);
         fflush (stdout);
      }
      
      * keylog_C = 0;                            /* start with a null string */
      
      for (cnt = 0, pt = keylog_C; cnt < num; cnt ++, pt ++)
      {
         * pt = digp.dg_value [cnt];
         if (debug_I == 44)
         {
            printf ("Digit rx = %c, digit type = %d\n", * pt, digp.dg_type [cnt]);
            fflush (stdout);
         }
      }
      
      * pt = (char) 0;
      
      if (num)
         dig_stat_I = USR_DIG_ENTERED;
      else
         dig_stat_I = USR_DIG_NO_ENTERED;
      
   
   return (ret);
}


/*************************************************************************
**
** name:           DLPlayExt.c
**
** purpose:        Play the indicated audio segment.
**                 Details included in the call.
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:     enterKey = DM_0 - DM_9, DM_S (*) DM_P (#), or 0 (no enter key)
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**             8/5 Modified to check the supplied file name.   If it ends in ".vox"
**                 then check to see if there exists a file with a similar file name ".vox8".
**                 If the .vox8 file exists, play it (at 8khz sample rate), else play the 
**                 originally requested file at 6khz.
**
**************************************************************************/

int DLPlayExt (char * fname, int maxSeconds, int maxDigits, int enterKey, int ignoreInput)

{
char * pt;
int cnt, num, ret;
long term;
DX_IOTT iott;

DV_TPT * tptp;

             /* note DIGMASK is always first, to be modified */

       // type, term type, length, flags, data, reserved, next ptr

DV_TPT universal_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_DIGMASK,0,TF_DIGMASK,0,0},
	{IO_CONT,DX_MAXDTMF,1,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_EOT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0}
};	
	
DV_TPT no_key_tpt [MAXTPTTERMS] = 
{ 
	{IO_CONT,DX_MAXDTMF,50,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_EOT,DX_MAXTIME,MAX_RECORD_PLAY_TIME,TF_MAXTIME,0,0},
};	
	
DV_TPT getdigit_tpt [MAXTPTTERMS] = 
{ 
	//{IO_CONT,DX_MAXDTMF,100,TF_MAXDTMF,0,0},
	{IO_CONT,DX_LCOFF,1,TF_LCOFF,0,0},
	{IO_CONT,DX_MAXTIME,1,TF_MAXTIME,0,0},
	{IO_EOT,DX_IDDTIME,1,TF_MAXTIME,0,0}
};	
	
DV_DIGIT digp;
char * pSuffix;		// prefix pointer
int bVoxSuffix = FALSE;
char vox8FileName [1024];
int bUseVox8 = FALSE;

unsigned short iReadOptions = EV_SYNC | PM_SR6;


	if (debug_I)
		printf ("DLPlayExt (%s, %d seconds, %d digits, EnterKey = %d, Ignore = %d)\n", 
			fname, maxSeconds, maxDigits, enterKey, ignoreInput);

   /* set termination parameters */

#ifdef SOON
   universal_tpt [3].tp_data = maxSeconds * 10;    // 100 ms ticks
#endif
   universal_tpt [1].tp_length = maxDigits;   

   if (enterKey)
      universal_tpt [0].tp_length = enterKey;

   if (ignoreInput)
      tptp = no_key_tpt;
   else
      tptp = universal_tpt;

   memset ((void *) & iott, 0, sizeof (iott));
   iott.io_type = IO_DEV | IO_EOT;
   iott.io_bufp = 0;
   iott.io_offset = (unsigned long) 0;
   iott.io_length = -1;          /* play to eof */


      // First extract the suffix including the '.', compare it to ".vox"
   pSuffix = strrchr (fname, '.');

   if (pSuffix)
   {
	    // if .vox suffix set a flag
      if (! strcmp (pSuffix, ".vox"))
      {
         bVoxSuffix = TRUE;
      }
   }
   // else no suffix



       //  If filename ends in ".vox"
       // first check for *.vox8,  if DNE, then use .vox
   if (bVoxSuffix)
   {
   struct stat statBuf;
   int ret;
   int fNameMainLen = pSuffix - fname;

	   // build vox8 file name to check

        strncpy (vox8FileName, fname, fNameMainLen);
	strcpy (vox8FileName + fNameMainLen, ".vox8");

	ret = stat (vox8FileName, & statBuf);

	   // if "*.vox8" exists...
	if (ret == 0)
	{
	   bUseVox8 = TRUE;
	}
	else
	{	
	}
   }
 //  else
	// use the supplied fname


	// if the .vox8 file exists, use it
   if (bUseVox8)
   {
      fname = vox8FileName;
      iReadOptions = EV_SYNC | PM_SR8;
      printf ("\tPlaying %s at 8khz\n", fname);
   }
   else
   {
      printf ("\tPlaying %s at 6khz\n", fname);
   }
 
   if ((iott.io_fhandle = open (fname, O_RDONLY)) == -1)
   {
      printf ("Error opening %s\n", fname);
      return (0);
   }

   if (debug_I)
   {
      //printf ("%s opened, dx_play called\n", fname);
      fflush (stdout);
   }

   ret = dx_play (chdev, & iott, tptp, iReadOptions);

   if (debug_I)
   {
      if (ret)
          printf ("DLPlayExt (): dx_play returned with %d !\n", ret);
      fflush (stdout);
   }

   term = ATDX_TERMMSK (chdev);

   if (term == TM_LCOFF)
	ret = -1; 
                        
   DebugATDX_TERMSK ("DLPlayEx"); 

   if (term & TM_MAXDTMF || term & TM_DIGIT || term & TM_EOD) /* key (s) expected) */
   {
      if (debug_I)
      {
         // printf ("Pre DLPlayExt() dx_getdig ()\n");
         // fflush (stdout);
      }

      num = dx_getdig (chdev, getdigit_tpt, & digp, EV_SYNC);
      
      num --;
      
      if (debug_I)
      {
         printf ("DLPlayExt() dx_getdig() indicates that there is %d digits entered\n", num);
         fflush (stdout);
      }
      
      * keylog_C = 0;                            /* start with a null string */
      
      for (cnt = 0, pt = keylog_C; cnt < num; cnt ++, pt ++)
      {
         * pt = digp.dg_value [cnt];
         if (debug_I == 44)
         {
            printf ("Digit rx = %c, digit type = %d\n", * pt, digp.dg_type [cnt]);
            fflush (stdout);
         }
      }
      
      * pt = (char) 0;
      
      if (num)
         dig_stat_I = USR_DIG_ENTERED;
      else
         dig_stat_I = USR_DIG_NO_ENTERED;
      
   }
   else
   {
      if (debug_I)
      {
         printf ("play received 0 keys during play\n");
         fflush (stdout);
      }
      
      dig_stat_I = USR_DIG_NO_ENTERED;
   }
   
   close (iott.io_fhandle);

   if (postPlayFn)
      (* postPlayFn) ();
   
   return (ret);
}


/*************************************************************************
**
** name:           DLForward.c
**
** purpose:        Dial (play the forward text string).
**
** return
** codes:          dx_dial code
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 12/98 - ds
**
**************************************************************************/

DLForward (char * numb)

{
DX_CAP cap;

  dx_clrcap (& cap);

  return dx_dial (chdev, numb, & cap, (unsigned short) EV_SYNC);
}


/*************************************************************************
**
** name:           DLGetKeyString.c
**
** purpose:        Return the caller entered keys.
**
** return
** codes:          Keys, or ""
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

char * DLGetKeyString ()

{

   if (dig_stat_I == USR_DIG_ENTERED)
   {
      return (keylog_C);
   }

   return ("");
}


DX_EBLK waitRing_DXEBLK;

DX_EBLK * DLReturnWaitRingDX_EBLK ()

{

   return & waitRing_DXEBLK;
}


/*************************************************************************
**
** name:           DLWaitRing.c
**
** purpose:        Wait until a call is received, or timeout.
**                 (not answering)
**
** return
** codes:          SUCCESS or ERROR, or -1 (TIMEOUT)
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLWaitRing (int seconds)

{
int ret;

    memset ((char *) & waitRing_DXEBLK, 0, sizeof (waitRing_DXEBLK));

    ret = dx_setevtmsk (chdev, DM_RINGS);

    //printf ("dx_setevtmsk () ret %d\n", ret);

    if (ret)
	return (ERROR);

       /*
       * Set to answer after MAXRING rings
       */
      if ( dx_setrings( chdev, 1) == -1 ) {
      //   fprintf( stderr, "dx_setrings() failed for %s",
       //         ATDV_NAMEP( chdev ) );

	 return (ERROR);
      }

    //printf ("Pre dx_getevt ()\n");
    ret = dx_getevt (chdev, & waitRing_DXEBLK, seconds);
    //printf ("Post dx_getevt () ret %d, %s\n", ret,
    //	ret == -1 ? "No Call" : "Call Received");

    /* process error */
    if (ret == -1)
    {
#define TESTIONLY
#ifdef TESTIONLY
               /* check if timed out */
        if (ATDV_LASTERR(chdev) == EDX_TIMEOUT)
        {
           printf("Timed out waiting for event.\n");
        }
        else
        {
           printf("Other Event Error.\n");
        }
#endif

	return (-1);
    }

    return SUCCESS;
}


/*************************************************************************
**
** name:           DLWaitRingCallerID.c
**
** purpose:        Wait until a call is received, or timeout.
**                 Wait until the second ring to attempt to get the Caller ID 
**                 information
**                 (not answering)
**
** return
** codes:          SUCCESS or ERROR, or -1 (TIMEOUT)
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLWaitRingCallerID (int seconds)

{
char buffer [100];
unsigned short parmval;
int ret;


	   // enable caller id    
	parmval = DX_CALLIDENABLE;

	if (dx_setparm (chdev, DXCH_CALLID, (void *) & parmval) == -1)
	{
		if (debug_I)
			fprintf (stderr, "DLWaitRingCallerID (), dx_setparam () Failed\n");

		return ERROR;
	}

	ret = dx_wtcallid (chdev, 2, seconds, buffer);

	if (ret == -1)
	{
		if (debug_I)
		{
		int err;
	
			fprintf (stderr, "DXWaitRingCallerID (), dx_wtcallid () ret -1\n");
			fprintf (stderr, "\t(%s)\n", ATDV_ERRMSGP (chdev));

			err = ATDV_LASTERR (chdev);

			if (err == EDX_CLIDINFO 	//  caller ID info/sub-msg not available
			|| err == EDX_CLIDBLK 		//  caller ID private ('P') or blocked
			|| err == EDX_CLIDOOA)		//  caller ID out of area ('O')
				return (SUCCESS);
		}

		return ERROR;
	}

	return SUCCESS;
}


int DLGetCallerIDCallingNumber (char * buffer)

{

	if (dx_gtextcallid(chdev,MCLASS_DN, buffer) == -1)
	{
        	strcpy (buffer, ATDV_ERRMSGP(chdev));
		return (ERROR);
	}
	return (SUCCESS);
}


int DLGetCallerIDDialedNumber (char * buffer)

{

	if (dx_gtextcallid(chdev,MCLASS_DDN, buffer) == -1)
	{
        	strcpy (buffer, ATDV_ERRMSGP(chdev));
		return (ERROR);
	}

	return (SUCCESS);
}


/*************************************************************************
**
** name:           DLAnswerRingingCall.c
**
** purpose:        Answer the call.
**                 No checking, the "phone" is expected to be ringing now.
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLAnswerRingingCall () 

{
int ret;

      ret = dx_sethook (chdev, DX_OFFHOOK, EV_SYNC);

   return (ret == 0 ? SUCCESS : ERROR);
}


/*************************************************************************
**
** name:           DLWaitCall.c
**
** purpose:        Wait until a call is received, or timeout.
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLWaitCall (int seconds)

{
char * pt;
int ret;
long lineState;

#ifdef STAT_TRACE
      fprintf (stderr, "\n\nDLWaitCall (%d)\n", seconds);
      fflush (stderr);
#endif

   lineState = ATDX_LINEST (chdev);

#ifdef STAT_TRACE
   fprintf (stderr, "                                "); 
   fprintf (stderr, ! (lineState & RLS_SILENCE) ? "SILENCE " : "silence "); 
   fprintf (stderr, (lineState & RLS_DTMF) ? "DTMF " : "dtmf "); 
   fprintf (stderr, ! (lineState & RLS_LCSENSE) ? "LCSENSE " : "lcsense "); 
   fprintf (stderr, ! (lineState & RLS_RING) ? "RING " : "ring "); 
   fprintf (stderr, ! (lineState & RLS_HOOK) ? "HOOK " : "hook "); 
   fprintf (stderr, "\n"); 
   fflush (stderr);

#endif
   
   if (! (lineState & RLS_RING))
   {
#ifdef STAT_TRACE
      fprintf (stderr, "   Ringing, --> OffHook\n");
      fflush (stderr);
#endif

      last_call_ret_I = ret = dx_sethook (chdev, DX_OFFHOOK, EV_SYNC);
   }
   else
   {
#ifdef STAT_TRACE
      fprintf (stderr, "   Not Ringing, Waiting for call ...\n");
      fflush (stderr);
#endif

      last_call_ret_I = 
         ret = dx_wtring (chdev, wtring_rings_I, DX_OFFHOOK, seconds);
   }
   
#ifdef STAT_TRACE
      if (ret == -1)
         fprintf (stderr, "      NO Call\n");
      else
         fprintf (stderr, "      Call Received!\n");
      fflush (stderr);
#endif

   if (ret == -1)
   {
      if (debug_I)
      {
         pt = ATDV_ERRMSGP (chdev);
         
         printf ("ring wait err: %s\n", pt);
         fflush (stdout);
      }
      
      return (0);
   }

   return (1);
}


/* WaitCallErrString.c */
char * WaitCallErrString ()

{

   if (last_call_ret_I == -1)
      return (ATDV_ERRMSGP (chdev));
   
   return ("No Wait Ring Error");
}


/*************************************************************************
**
** name:           DLCallerId.c
**
** purpose:        Enable or Disable the caller id feature
**
** return
** codes:          SUCCESS or ERROR
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

DLCallerId (int bEnable)


//    NOTE, not proven/tested



{
unsigned short parmval;
int ret;

   ret = dx_setparm (chdev, DXCH_CALLID, (void *) & parmval);
   if (ret == -1)
      return ERROR;

   ret = dx_setevtmsk (chdev, DM_RINGS);
   if (ret == -1)
      return ERROR;

   return SUCCESS;
}


/*************************************************************************
**
** name:           DLInit.c
**
** purpose:        Initialize the requested dialogic card port.
**
** return
** codes:          DL_INIT_SUCCESS, DL_INIT_FAIL, or DL_INIT_BUSY
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

DLInit (char * port, int debug)

{
int ret;

   debug_I = debug;
   strcpy (port_C, port);
   
   if (debug_I)
   {
      printf ("DLInit (%s)  (debugging enabled)\n", port);
      fflush (stdout);
   }
   
   ret = DLLockPort ();
   
   if (debug_I)
   {
      //printf ("DLInit: DLLockPort () ret %d\n", ret);
      //fflush (stdout);
   }
      
   if (ret == SUCCESS)
   {
      if (debug_I)
      {
         printf ("DLInit: Calling dx_open (%s, 0)\n", port);
         fflush (stdout);
      }
      
      chdev = dx_open (port, 0);    /* ex: "dxxxB1C1", 0);   board 1 channel 1 */
   
      if (debug_I)
      {
         printf ("DLInit: dx_open () returns  %d\n", chdev);
         fflush (stdout);
      }

      if (chdev == -1)
         return (DL_INIT_FAIL);
      
      dx_sethook (chdev, DX_ONHOOK, EV_SYNC);

      return (DL_INIT_SUCCESS);
   }
   
   return (DL_INIT_BUSY);
}


/*************************************************************************
**
** name:           DLInstallQuit.c
**
** purpose:        Install the interrupt trap for DLQuit ().
**
** return
** codes:          na
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

void DLInstallQuit ()

{

   signal (SIGINT, DLQuit);
}


#ifdef TESTING
main (int argc, char ** argv)

{
char * pt;
int ret;

   for (ret = 1; ret < argc; ret ++)
      if (! strcmp (argv [ret], "-d"))
      {
         debug_I = 1;
         break;
      }

   chdev = dx_open ("dxxxB1C1", 0);
   
   if (debug_I)
   {
      printf ("open  ret  %d\n", chdev);
      fflush (stdout);
   }

   if (chdev != -1)
   {
      dx_sethook (chdev, DX_ONHOOK, EV_SYNC);
   
      printf ("Waiting for ring ... (20 sec max)\n");
   
      ret = dx_wtring (chdev, 1, DX_OFFHOOK, 20);
      printf ("wtring  ret  %d\n", ret);

      if (ret == -1)
      {
         pt = ATDV_ERRMSGP (chdev);
         printf ("ring wait err: %s\n", pt);
      }
      else               /* ring, answer */
      {
         AudioMain (argc, argv);
         printf ("call, offhook, calling Play\n");

         ret = Play (chdev, "greet.vox");
         
         printf ("Play returns, with %d, calling Record\n", ret);
         
         ret = Record (chdev, "new.vox");

         printf ("Record returns, with %d\n", ret);
         
         ret = Play (chdev, "thanks.vox");
      }
#endif



int DLIsOffHook ()

{
int offHook = 0;
long lineState;
                                                 /* check for off hook */
                                                 /* or loop current */
                                                 /* either is off hook condition */

// originally my understanding was that if EITHER the hook state, or the loop current indicators
// showed off hook, then the line was off hook.

// But testing on a SO Bell line showed, both indicated off hook correctly, but when
// the caller hung up, the RLS_HOOK was still showing off hook (incorrectly)
//
// So for the moment, if BOTH show off hook, then OFF hook, else, ON hook.
//

	
   lineState = ATDX_LINEST (chdev);              /* get current line state */

   if (debug_I)
   {
      printf ("   DLIsOffHook () RLS_HOOK: %s, RLS_LCSENSE: %s\n",
		lineState&RLS_HOOK ? "ON Hook" : " OFF Hook", lineState&RLS_LCSENSE ? "ON Hook" : "OFF Hook");
      fflush (stdout);
   }


   if (! (lineState & RLS_HOOK))
      offHook ++;
   
   if (! (lineState & RLS_LCSENSE))
      offHook ++;

   return offHook == 2;	
}

   
/*************************************************************************
**
** name:           DLHangUp.c
**
** purpose:        Clear the dialogic port, and hang up the phone line.
**
** return
** codes:          na
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLHangUp ()

{
int setret = 0, hangUpReq = FALSE;
int ret;
long lineState;

   if (debug_I)
   {
      printf ("DLHangUp ()...  ");
      fflush (stdout);
   }

   if (ATDX_STATE (chdev) != CS_IDLE)
   {
      printf ("             ATDX_STATE () != CS_IDLE, == %x, dx_stopch () required\n", ATDX_STATE (chdev));
      fflush (stdout);
     
      ret = dx_stopch (chdev, EV_ASYNC);                   /* terminate play/record */
   }
 
   if (debug_I)
   {
      printf ("Post dx_stopch (%d, EV_ASYNC) (%s)\n", chdev, ret == 0 ? "SUCCESS" : "ERROR");
      fflush (stdout);
   }


#ifdef EARLIER_CODE
   lineState = ATDX_LINEST (chdev);              /* get current line state */

   if (debug_I)
   {
      printf ("DLHangUp (), post dx_stopch (), ATDX_LINEST = %s\n", lineState & RLS_HOOK ? "On Hook" : "Off Hook");
      fflush (stdout);
   }
   
                                                 /* check for off hook */
                                                 /* or loop current */
                                                 /* either is off hook condition */
   if (! (lineState & RLS_HOOK))
   {
      hangUpReq = TRUE;

      if (debug_I)
      {
         printf ("   lineState & RLS_HOOK  ->  dx_sethook () required\n");
         fflush (stdout);
      }
   }
   
   if (! (lineState & RLS_LCSENSE))
   {
      hangUpReq = TRUE;

      if (debug_I)
      {
         printf ("   lineState & RLS_LCSENSE  ->  dx_sethook () required\n");
         fflush (stdout);
      }
   }
   if (hangUpReq)
#endif


   if (1 || DLIsOffHook ()) 
   {
      if (0 && debug_I)
      {
         printf ("   dx_sethook (DX_ONHOOK) executing...");
         fflush (stdout);
      }

      setret = dx_sethook (chdev, DX_ONHOOK, EV_SYNC);
      
      if (0 && debug_I)
      {
         printf ("DONE\n");
         fflush (stdout);
      }
   }
   else
   {
	if (0 && debug_I)
	{
  	    printf ("      dx_sethook (DX_ONHOOK) NOT required\n");
  	    fflush (stdout);
	}
   }

   dx_clrdigbuf (chdev);
   
   return (setret == 0);
}


/*************************************************************************
**
** name:           DLCleanup.c
**
** purpose:        Close the dialogic port properly
**
** return
** codes:          na
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

void DLCleanup ()

{
int setret, ret;

   if (debug_I)
   {
      fprintf (stderr, "DLCleanup (), pre DLHangUp()\n");
      fflush (stderr);
   }

   setret = DLHangUp ();

   if (debug_I)
   {
      fprintf (stderr, "DLCleanup (), pre dx_close (chdev)\n");
      fflush (stderr);
   }
   dx_close (chdev);
   
   if (setret != 0)                              /* if sethook failed */
   {
      if (debug_I)
      {
         fprintf (stderr, "DLCleanup () pre dx_open () (sethook failed)\n");
         fflush (stderr);
      }
      chdev = dx_open (port_C, 0);               /* reopen, hangup and close */
      DLHangUp ();
      
      dx_close (chdev);
   }

   DLUnLockPort ();
}


/*************************************************************************
**
** name:           DLQuit.c
**
** purpose:        The interrupt trap cleanup and exit the pgm function
**
** return
** codes:          
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

void DLQuit (int trash)                          /* to be called on abort */

{
static int count = 0;

//   signal (SIGINT, SIG_IGN);

   if (++ count > 1)
      exit (0);


   if (debug_I)
   {
	fprintf (stderr, "DLQuit () Entered\n");
        fflush (stderr);
   } 

   if (debug_I)
   {
	   fprintf (stderr, "Calling DLCleanup () from DLQuit ()\n"); 
           fflush (stderr);
   } 

   DLCleanup ();

   if (debug_I)
   {
	  fprintf (stderr, "Post DLCleanup ()\n");
          fflush (stderr);
   } 
 
   exit (0);
}


/*************************************************************************
**
** name:           DLSetPlayTerm.c
**
** purpose:        Save the new play mode
**                 Valid;  PLAY_TERM_ANY_KEY PLAY_TERM_POUND_KEY PLAY_TERM_STAR_KEY
**                         PLAY_TERM_NO_KEY  (ignore presses)
**
** return
** codes:          na
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 9/3 - ds
**
**************************************************************************/

int DLSetPlayTerm (int stat)

{

   play_term_I = stat;

   return (SUCCESS);
}


/*************************************************************************
**
** name:           DLHungUp.c
**
** purpose:        Decide if line is still active
**
** return
** codes:          
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 7/97 - ds
**
**************************************************************************/

int DLHungUp ()                                  /* not usable */

{
int ret;

   ret = ATDX_LINEST (chdev);

   printf ("    LINEST = %d\n", ret);
   
   if (ret == AT_FAILURE)
   {
      if (debug_I)
      {
         printf ("DLHungUp: ERROR in ATDX_LINEST ()\n");
         fflush (stdout);
      }
         
      return (-1);
   }

   if (ret & RLS_LCSENSE)      /* lost line */
   {
      if (debug_I)
      {
         printf ("Caller hung up\n");
         fflush (stdout);
      }
       
      return (1);
   }
   
   if (debug_I)
   {
      printf ("Caller still on line\n");
      fflush (stdout);
   }
        
   return (0);
}


