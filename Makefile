#
#          Makefile for sch the school closing file
#



#     use: make xxxx -e DEBUG=-g      to compile xxxx -g

INCLUDE_DIRS = -I./

MAKEFILE = Makefile

DEBUG = -g

OPTIONS = $(INCLUDE_DIRS) -c -DNO_DEBUG_CODE $(DEBUG)
LIB = 

libdl.a:	libdl.o
		ar rv libdl.a libdl.o
		
libdl.o:	libdl.c $(INCLUDES) $(MAKEFILE)
		cc $(OPTIONS) libdl.c

libxml.o:	libxml.c $(INCLUDES) $(MAKEFILE)
		cc $(OPTIONS) libxml.c

term:	term.c
	cc $(INCLUDE_DIRS) term.c libdl.o $(LIB) \
#		-ldti -ldxxx -lsrl -o term
		-ldxxmt -lsrlmt -o term
#	rcp term dcm3:dl

dlsrv:		dlsrv.o libdl.o locks.o libxml.o
		cc dlsrv.o libdl.o locks.o libxml.o $(LIB) \
			-ldti -ldxxx -lsrl -o dlsrv 

msgrec:		msgrec.o libdl.o locks.o
		cc msgrec.o libdl.o locks.o $(LIB) \
			-ldti -ldxxx -lsrl -o msgrec

xmltest:	xmltest.o libxml.o
		cc xmltest.o libxml.o $(LIB) \
			-ldti -ldxxx -lsrl -o xmltest

play:		play.o libdl.o locks.o
		cc play.o libdl.o locks.o $(NOVIEWS) $(LIB) \
			-ldti -ldxxx -lsrl -o play

ltest:		dltest.o libdl.a
		cc $(INCLUDE_DIRS) dltest.o libdl.a $(LIB) \
			-ldti -ldxxx -lsrl -o dltest

locks.o:	locks.c $(MAKEFILE)
		cc $(OPTIONS) locks.c

msgrec.o:	msgrec.c $(MAKEFILE)
		cc $(OPTIONS) msgrec.c

dlsrv.o:	dlsrv.c $(MAKEFILE)
		cc $(OPTIONS) dlsrv.c

play.o:		play.c $(MAKEFILE)
		cc $(OPTIONS) play.c

dltest.o:	dltest.c $(MAKEFILE)
		cc $(OPTIONS) dltest.c

clean:
	-rm *.o dlsrv msgrec play ltest
