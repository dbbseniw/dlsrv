
#define ALLOCATE

#include <stdio.h>
#include "libdl.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <locks.h>

#include <srllib.h>
#include <dxxxlib.h>   // testing only

static char VER [] = "2.0";
static char LockText_C [100];

FileExists(fname,notify)
char *fname;
int notify;
{   
struct stat buf;   
char ebuf[80], *StripPath();
int ret;     

   if((stat(fname,&buf)) == 0)              /* file found ok */  
       ret = SUCCESS;
   else
      ret = ERROR;
   return(ret);
}

extern int chdev;

main (int argc, char ** argv)
 
{
char buf [100], * pt;
int ret;

DX_EBLK dx_EBlk;

    fprintf (stderr, "\nmsgrec ver %s\n\n", VER);

    ret = DLInit ("dxxxB1C1", 1);

    if (ret == FILEBUSY)
    {
       fprintf (stderr, "Dialogic port is being used!\n");
       exit (0);
    }

    if (ret != SUCCESS)
    {
       fprintf (stderr, "Initialization of Dialogic board failed!\n");
       exit (0);
    }

    DLInstallQuit ();


#ifdef TESTING_RING_ONLY

    ret = dx_setevtmsk (chdev, DM_RINGS);

    printf ("dx_setevtmsk () ret %d\n", ret);


       /*
       * Set to answer after MAXRING rings
       */
      if ( dx_setrings( chdev, 1) == -1 ) {
         fprintf( stderr, "dx_setrings() failed for %s",
                ATDV_NAMEP( chdev ) );
         //QUIT( 2 );
      }


    ret = dx_getevt (chdev, & dx_EBlk, 20);

    printf ("dx_getevt () ret %d\n", ret);

    /* process error */
    if (ret == -1)
    {
               /* check if timed out */
        if (ATDV_LASTERR(chdev) == EDX_TIMEOUT) 
        { 
           printf("Timed out waiting for event.\n");
        }
        else 
        {
           printf("Other Event Error.\n");
        }
    }


#endif
 
    printf ("\n\nPhone the Newsroom Solutions system to record voice messages.\n");

    sprintf (LockText_C, "Msgrec version %s waiting for a call", VER);
    DLLockFileText (LockText_C, DL_PHONE_WAITING_FOR_CALL);

    for (;;)
       if (DLWaitCall (10))
          break;

    printf ("\nCall received\n\n");
    
    DLLockFileText ("Msgrec: call received", DL_PHONE_CALL_IN_PROGRESS);
    
    for (;;)
    {
       DLLockFileText ("Msgrec: waiting for next command", DL_PHONE_CALL_IN_PROGRESS);
       
       DLPlay (".entrname.vox");
       
       gets (buf);
       
       printf ("\n");
    
       if (strlen (buf))
       {
       char fname [100];
       
          sprintf (fname, "%s.vox", buf);

          if (FileExists (fname, FALSE))
          {
             sprintf (LockText_C, "Msgrec: replacing vox file %s", fname);
             DLLockFileText (LockText_C, DL_PHONE_CALL_IN_PROGRESS);

             DLPlay (".currentmsg.vox");
          
             DLPlay (fname);
          }
          else
          {
             sprintf (LockText_C, "Msgrec: creating new vox file %s", fname);
             DLLockFileText (LockText_C, DL_PHONE_CALL_IN_PROGRESS);
             
             DLPlay (".newmsg.vox");
          }

          sprintf (LockText_C, "Msgrec: recording vox file %s", fname);
          DLLockFileText (LockText_C, DL_PHONE_CALL_IN_PROGRESS);
             
          DLPlay (".speakmsg.vox");

          printf ("Now recording the message '%s'\n\n", fname);
          
          DLRecord (fname);

          printf ("Playing recording\n");
          
          DLPlay (fname);
          
          printf ("Next recording name?\n");
          
          DLPlay (".ctrlc.vox");
       }
    }
 }
 
 
