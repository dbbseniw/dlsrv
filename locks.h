/*************************************************************************
**
** name:           locks.h
**
** call:           n.a.
**
** purpose:        include file for locks.c
**
** return
** codes:          na
**
** assumptions:    
**
** externals:      
**
** author:         dave seniw
**
** revision
** history:        original version - 09/3 - ds
**
**************************************************************************/

#ifndef __LOCKS_H
#define __LOCKS_H

#define SUCCESS          1
#define ERROR            0

#ifdef MSDOS

#define F_RDLCK          1                   /* define these for dos */
#define F_WRLCK          2                   /* even though no locks */
#define F_UNLCK          3

#endif

#ifdef ALLOCATE

char BusyFileMsg[]   = "File In Use, Please Wait";
char CntLckFl[]      = "FATAL ERROR: Cannot Set File Lock";

#else

extern char BusyFileMsg[];
extern char CntLckFl[];

#endif

#define FILEBUSY         -1

int SetLock(char * fname,FILE * fp,int type,int retries,int notify);
int SetLockQuiet (char * fname,FILE * fp,int type,int retries,int notify);
int CkLock(char * fname, FILE * fp, int type, int retries, int notify);
int LockPort (int fd,int type,int retries,int notify);
int LockPortBlock (int fd, int type, int notify);







#endif  /* __LOCKS_H */
