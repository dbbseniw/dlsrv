/*************************************************************************
**
** name:          SetLock()
**
** call:          SetLock(fname,fp,type,retries,notify)
**                char *fname;  - name of file to lock, DOS ONLY
**                FILE *fp;     - pointer to open file
**                int type;     - type of lock
**                int retries;  - number of retries
**                int notify;   - notify user of error
**
** purpose:       put lock on a file lock it.
**
** return
** codes:         if ret == SUCCESS - all ok
**                          FILE BUSY if file busy
**                   
**
** assumptions:   - type for now will DEFINED TYPES F_RDLCK AND
**                  F_WRLCK for read and write.
**                - this function locks entire file
**
** externals:
**
** revision
** history:       original version - 9/3 - ds
**
**************************************************************************/


#include <stdio.h>
#include <fcntl.h>
#include "locks.h"

#define TRUE                1
#define FALSE               0
#define NULLP               (char *) 0
#define NOTIFY              1
#define NONOTIFY            0


UsrMsg (int a, int b, char * c, void * d)
{
}

ErrorMsg(char *func,char *CntLckFl,int a, int b, int c)
{
}

SetLock(char * fname,FILE * fp,int type,int retries,int notify)
{
   int fd, ret, cnt, msgup;
   struct flock lck;
   static char func[] = "SetLock";

   msgup = cnt = 0;                         /* zero this out */

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   do
   {
      ret = fcntl(fd,F_SETLK,&lck);         /* try to lock file */

      if(ret < 0)                           /* could not lock */
      {
         if(msgup == 0)                     /* message not displayed */
         {
            UsrMsg(TRUE,1,BusyFileMsg,NULLP);
            msgup = 1;                      /* do it, set flag */
         }

         sleep(1);                          /* so wait */
         cnt++;
      }
   }
   while((ret < 0) && (cnt < retries));

   if(msgup)                                /* message was put up */
      UsrMsg(FALSE,0,NULLP,NULLP);          /* take it down */

   if(ret < 0)                              /* lock failed */
   {
      if(notify == NOTIFY)                  /* notify user ? */
         ErrorMsg(func,CntLckFl,TRUE,TRUE,FALSE);

      ret = FILEBUSY;                       /* set error */
   }

   return(ret);
}


/*************************************************************************
**
** name:          SetLockQuiet()
**
** call:          SetLockQuiet(fname,fp,type,retries,notify)
**                char *fname;  - name of file to lock, DOS ONLY
**                FILE *fp;     - pointer to open file
**                int type;     - type of lock
**                int retries;  - number of retries
**                int notify;   - notify user of error
**
** purpose:       Just like setlock, but with no error messages
**
** return
** codes:         if ret == SUCCESS - all ok
**                          FILE BUSY if file busy
**                   
**
** assumptions:   - type for now will DEFINED TYPES F_RDLCK AND
**                  F_WRLCK for read and write.
**                - this function locks entire file
**
** externals:
****
** revision
** history:       original version - ds
**
**************************************************************************/

SetLockQuiet (char * fname,FILE * fp,int type,int retries,int notify)
{
   int fd, ret, cnt, msgup;
   struct flock lck;
   static char func[] = "SetLock";

   msgup = cnt = 0;                         /* zero this out */

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   do
   {
      ret = fcntl(fd,F_SETLK,&lck);         /* try to lock file */

      if(ret < 0)                           /* could not lock */
      {
         sleep(1);                          /* so wait */
         cnt++;
      }
   }
   while((ret < 0) && (cnt < retries));

   if(ret < 0)                              /* lock failed */
   {
      if(notify == NOTIFY)                  /* notify user ? */
         ErrorMsg(func,CntLckFl,TRUE,TRUE,FALSE);

      ret = FILEBUSY;                       /* set error */
   }

   return(ret);
}


/* CkLock.c */
CkLock(char * fname,FILE * fp,int type,int retries,int notify)
{
   int fd, ret, cnt, msgup;
   struct flock lck;
   static char func[] = "CkLock";

   msgup = cnt = 0;                         /* zero this out */

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   ret = fcntl(fd,F_GETLK,&lck);         /* try to lock file */

   if(ret < 0)                           /* could not lock */
   {
      return (FALSE);
   }

   return (lck.l_type != F_UNLCK);
}


/*************************************************************************
**
** name:          RecLock()
**
** call:          RecLock(fname,fp,type,start,len,retries,notify)
**                char *fname;  - DOS ONLY
**                FILE *fp;     - pointer to open file
**                int type;     - type of lock
**                long start    - first character in the record to lock
**                long len      - length of the record to lock
**                int retries;  - number of retries
**                int notify;   - notify user of error
**
** purpose:       put lock on a file record.
**                if retries == 0, no sleeping is executed or messages are 
**                displayed
**
** return
** codes:         if ret == SUCCESS - all ok
**                          FILE BUSY if file busy
**                   
**
** assumptions:   - type for now will DEFINED TYPES F_RDLCK AND
**                  F_WRLCK for read and write.
**                - this function locks file records
**
** externals:
**
** revision
** history:       original version - 9/3 - ds
**
**************************************************************************/

RecLock(fname,fp,type,start,len,retries,notify)
char *fname;
FILE *fp;
int type;
long start;
long len;
int retries;
int notify;
{
   int fd, ret, cnt, msgup;
   struct flock lck;
   static char func[] = "RecLock";

   msgup = cnt = 0;                         /* zero this out */

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = start;
   lck.l_len = len;

   do
   {
      ret = fcntl(fd,F_SETLK,&lck);         /* try to lock the record */

      if(ret < 0 && retries > 0)             /* could not lock */
      {
         if(msgup == 0)                     /* message not displayed */
         {
            UsrMsg(TRUE,1,BusyFileMsg,NULLP);
            msgup = 1;                      /* do it, set flag */
         }

         sleep(1);                          /* so wait */
         cnt++;
      }
   }
   while((ret < 0) && (cnt < retries));

   if(msgup)                                /* message was put up */
      UsrMsg(FALSE,0,NULLP,NULLP);          /* take it down */

   if(ret < 0)                              /* lock failed */
   {
      if(notify == NOTIFY)                  /* notify user ? */
         ErrorMsg(func,CntLckFl,TRUE,TRUE,FALSE);

      ret = FILEBUSY;                       /* set error */
   }

   return(ret);
}



/*************************************************************************
**
** name:          RecUnLock()
**
** call:          RecUnLock(fname,fp,start,len)
**                char *fname;  - USED BY DOS ONLY
**                FILE *fp;     - pointer to open file
**                long start    - first character in the record to lock
**                long len      - length of the record to lock
**
** purpose:       remove a lock on a file record.
**
** return
** codes:         if ret == SUCCESS - all ok
**                   
**
** assumptions:   - type for now will DEFINED TYPES F_RDLCK AND
**                  F_WRLCK for read and write.
**                - this function unlocks file records
**
** externals:
**
** revision
** history:       original version - ds
**
**************************************************************************/

RecUnLock(fname,fp,start,len)
char fname;
FILE *fp;
long start;
long len;
{
   int fd;
   struct flock lck;

   fd = fileno(fp);                         /* get the fd */

   lck.l_type = F_UNLCK;
   lck.l_whence = 0;
   lck.l_start = start;
   lck.l_len = len;

   fcntl(fd,F_SETLK,&lck);                  /* unlock the record */

   return (SUCCESS);
}


/*************************************************************************
**
** name:          LockPort ()
**
** call:          LockPort (fd,type,retries,notify)
**                int fd;       - fd to the open port
**                int type;     - type of lock
**                int retries;  - number of retries
**                int notify;   - notify user of error
**
** purpose:       Lock or unlock a port.
**
** return
** codes:         if ret == SUCCESS - all ok
**                          FILE BUSY if port is busy
**                   
**
** assumptions:   - type for now will DEFINED TYPES F_RDLCK AND
**                  F_WRLCK for read and write.
**                - this function locks entire file
**
** externals:
**
** revision
** history:       original version - 9/3 - ds (originally from SetLock)
**
**************************************************************************/

LockPort (int fd,int type,int retries,int notify)

{

#ifndef MSDOS

   int ret, cnt, msgup;
   struct flock lck;
   static char func [] = "LockPort";

   cnt = 0;                                  /* zero this out */

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   do
   {
      ret = fcntl(fd,F_SETLK,&lck);         /* try to lock file */

      if(ret < 0)                           /* could not lock */
      {
         sleep(1);                          /* so wait */
         cnt++;
      }
   }
   while((ret < 0) && (cnt < retries));

   if(ret < 0)                              /* lock failed */
   {
      if(notify == NOTIFY)                  /* notify user ? */
         ErrorMsg(func,CntLckFl,TRUE,TRUE,FALSE);

      ret = FILEBUSY;                       /* set error */
   }
   else
      ret = SUCCESS;

   return(ret);

#else    /* for dos */

   return (SUCCESS);
   
#endif
}


/*************************************************************************
**
** name:          LockPortBlock ()
**
** call:          LockPortBlock (fd,type,notify)
**                int fd;       - fd to the open port
**                int type;     - type of lock
**                int notify;   - notify user of error
**
** purpose:       Lock or unlock a port.
**
** return
** codes:         if ret == SUCCESS - all ok
**                          FILE BUSY if port is busy
**                   
**
** assumptions:   - type for now will DEFINED TYPES F_RDLCK AND
**                  F_WRLCK for read and write.
**                - this function locks entire file
**
** externals:
**
** revision
** history:       original version - 9/3 - ds (originally from SetLock)
**
**************************************************************************/

LockPortBlock (int fd, int type, int notify)

{
int ret;
struct flock lck;
static char func [] = "LockPortBlock";

   lck.l_type = type;
   lck.l_whence = 0;
   lck.l_start = 0L;
   lck.l_len = 0L;

   ret = fcntl (fd, F_SETLKW, &lck);         /* try to lock file */

   if(ret < 0)                              /* lock failed */
   {
      if(notify == NOTIFY)                  /* notify user ? */
         ErrorMsg(func,CntLckFl,TRUE,TRUE,FALSE);

      ret = FILEBUSY;                       /* set error */
   }
   else
      ret = SUCCESS;

   return(ret);
}



